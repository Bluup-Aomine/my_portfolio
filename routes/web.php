<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ROUTES FRONT */
Route::get('/', 'HomeController@index')->name('index');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/services', 'HomeController@services')->name('services');
Route::get('/portfolio', 'HomeController@portfolio')->name('portfolio');
Route::get('/contact', 'ContactController@contact')->name('contact');
Route::post('/contact', 'ContactController@sendMail')->name('contact.send');

/* ROUTES ADMIN */
Route::get('/admin', 'AdminLoginController@admin')->name('admin');
Route::post('/admin', 'AdminLoginController@login')->name('admin.login');
Route::post('/admin/logout', 'AdminLoginController@logout')->name('admin.logout');
Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
Route::get('/admin/settings', 'SettingController@index')->name('settings.index');
Route::post('/admin/settings', 'SettingController@edit')->name('settings.edit');
/* ROUTES RESOURCE ADMIN */
Route::resource('admin/skills', 'SkillController');
Route::resource('admin/projects', 'ProjectController');
Route::resource('admin/categories', 'CategoryController');
