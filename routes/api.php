<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/skills', 'Api\SkillControllerApi@setDataSkills');
Route::get('/projects', 'Api\ProjectControllerApi@setDataProjects');
Route::get('/project/filterAll', 'Api\ProjectControllerApi@filterAll')->name('api.projects.filterAll');
Route::get('/project/filter/{slugFilter}', 'Api\ProjectControllerApi@filter')->name('api.projects.filter');
