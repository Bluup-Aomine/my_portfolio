<?php

namespace Project;

use App\Category;
use App\Skill;
use App\SkillProject;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Tests\Unit\admin\TestUnitUserCase;

class ProjectSkillsTest extends TestUnitUserCase
{
    public function testSkillsProjectIsNotArray()
    {
        $this->initializedUser();
        $this->setCategory();

        $category = Category::Find(1);
        $data = [
            'title' => 'Test de test',
            'content' => 'content de test',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2),
            'back_end' => 'test back end',
            'front_end' => 'test front end',
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'An error has been occurred when to creating project !');
        $this->assertStringContainsString('admin/projects/create', $request->getContent());
    }

    public function testSkillsProjectDataSkillBackDontExist()
    {
        $this->initializedUser();
        $this->setCategory();
        $this->setSkills();

        $category = Category::Find(1);
        $data = [
            'title' => 'Test de test',
            'content' => 'content de test',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2),
            'back_end' => ['Python'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'One of the chosen skills don\'t exists in our database !');
        $this->assertStringContainsString('admin/projects/create', $request->getContent());
    }

    public function testSkillsProjectSuccess()
    {
        $this->initializedUser();
        $this->setCategory();
        $this->setSkills();

        $category = Category::Find(1);
        $data = [
            'title' => 'Test de test',
            'content' => 'content de test',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->getContent());

        // Asserts Data Skills Project
        $skillProjectCount = SkillProject::count();
        $skillProject = SkillProject::find(2);
        $this->assertEquals(2, $skillProjectCount);
        $this->assertEquals(2, $skillProject->id);
        $this->assertEquals(1, $skillProject->project_id);
    }

    private function setCategory()
    {
        factory(Category::class, 1)->create();
    }

    private function setSkills()
    {
        for ($i = 0; $i < 2; $i++) {
            Skill::create([
               'name' => $i === 0 ? 'PHP' : 'JS',
               'percentage' => 80,
               'type' => 'language',
               'back_or_front' => $i === 0 ? 'back_end' : 'front_end'
            ]);
        }
    }
}
