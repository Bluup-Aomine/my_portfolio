<?php

namespace Project\API;

use App\Category;
use App\Project;
use Carbon\Carbon;
use Illuminate\Testing\TestResponse;
use Tests\Unit\admin\TestUnitUserCase;

class ProjectFilterTest extends TestUnitUserCase
{

    private array $data = [];

    public function testFilterParamsIsAString()
    {
        $this->data['slugFilter'] = 'test';
        $requestJSON = $this->setRequestFilterCategory();

        $this->assertIsString($this->data['slugFilter']);
        $requestJSON->assertStatus(302);
    }

    public function testFilterParamsIsDontAString()
    {
        $this->data['slugFilter'] = false;
        $requestJSON = $this->setRequestFilterCategory();

        $requestJSON->assertStatus(404);
    }

    public function testFilterCategoryNotExist()
    {
        $this->data['slugFilter'] = 'category';
        $requestJSON = $this->setRequestFilterCategory();
        $content = \GuzzleHttp\json_decode($requestJSON->getContent());

        $requestJSON->assertStatus(302);
        $this->assertEquals(false, $content->success);
        $this->assertEquals('This category don\'t exist in our database !', $content->message);
    }

    public function testFilterAnyProjects()
    {
        // Create a Category
        $this->setCategory();

        $this->data['slugFilter'] = 'test-de-test-1';
        $requestJSON = $this->setRequestFilterCategory();
        $content = \GuzzleHttp\json_decode($requestJSON->getContent());

        $requestJSON->assertStatus(302);
        $this->assertEquals(false, $content->success);
        $this->assertEquals('Any projects has been found with this category !', $content->message);
    }

    public function testFilterProjects()
    {
        // Create a Category and projects
        $this->setCategory();
        $this->setProject();

        $this->data['slugFilter'] = 'test-de-test-1';
        $requestJSON = $this->setRequestFilterCategory();
        $content = \GuzzleHttp\json_decode($requestJSON->getContent());

        $requestJSON->assertStatus(302);
        $this->assertEquals(true, $content->success);
        $this->assertIsArray($content->data);
    }

    public function testFilterAllProjectsEmpty()
    {
        // Create a Category and projects
        $this->setCategory();
        $this->setProject();

        $requestJSON = $this->setRequestFilterAll();
        $content = \GuzzleHttp\json_decode($requestJSON->getContent());

        $requestJSON->assertStatus(302);
        $this->assertEquals(true, $content->success);
        $this->assertIsArray($content->data);
        $this->assertEquals(5, count($content->data));
    }

    public function testFilterAll()
    {
        $requestJSON = $this->setRequestFilterAll();
        $content = \GuzzleHttp\json_decode($requestJSON->getContent());

        $requestJSON->assertStatus(302);
        $this->assertEquals(false, $content->success);
        $this->assertEquals('Any projects has been found !', $content->message);
    }

    /**
     * @return TestResponse
     */
    private function setRequestFilterCategory()
    {
        $this->initializedUser();

        $route = route('api.projects.filter', $this->data);
        return $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])->json('GET', $route);
    }

    /**
     * @return TestResponse
     */
    private function setRequestFilterAll()
    {
        $this->initializedUser();

        $route = route('api.projects.filterAll');
        return $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ])->json('GET', $route);
    }

    /**
     * @return void
     */
    private function setCategory(): void
    {
        for ($i = 0; $i < 2; $i++) {
            $index = ($i + 1);

            Category::create([
                'name' => 'test de test ' . $index,
                'slug' => 'test-de-test-' . $index,
                'content' => 'test de content ' . $index
            ]);
        }
    }

    private function setProject()
    {
        for ($i = 0; $i < 5; $i++) {
            $index = ($i + 1);

            Project::create([
                'title' => 'test title ' . $index,
                'slug' => 'test-title-' . $index,
                'content' => 'test content ' . $index,
                'category_id' => $index === 1 || $index === 3 ? 2 : 1,
                'start_project' => Carbon::now()->format('Y-m-d'),
                'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d')
            ]);
        }
    }

}
