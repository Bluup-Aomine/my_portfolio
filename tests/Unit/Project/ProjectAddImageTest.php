<?php

namespace Project;

use App\Category;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\TestResponse;
use Tests\Unit\admin\TestUnitUserCase;

class ProjectAddImageTest extends TestUnitUserCase
{
    public function testIfFileDontExist()
    {
        $this->initializedUser();
        $this->setCategory();

        $category = Category::find(1);
        $data = [
            'title' => 'test de title',
            'content' => 'test de content',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(3)->format('Y-m-d'),
            'image-test' => ''
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Assets Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'The field image don\'t exist !');
        $this->assertStringContainsString('admin/projects/create', $request->getContent());
    }

    public function testFieldImageDontFile()
    {
        $this->initializedUser();
        $this->setCategory();

        $category = Category::find(1);
        $data = [
            'title' => 'test de title',
            'content' => 'test de content',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(3)->format('Y-m-d'),
            'image' => 'test'
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Assets Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'An error has been occurred when to creating project !');
        $this->assertStringContainsString('admin/projects/create', $request->getContent());
    }

    public function testImageBadExtensions()
    {
        $this->initializedUser();
        $this->setCategory();

        $category = Category::find(1);
        $data = [
            'title' => 'test de title',
            'content' => 'test de content',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(3)->format('Y-m-d'),
            'image' => UploadedFile::fake()->create('avatar.gif')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Assets Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', "You file must be to type ('JPG, PNG, JPEG') !");
        $this->assertStringContainsString('admin/projects/create', $request->getContent());
    }

    public function testUploadImageDatabaseSuccess()
    {
        $request = $this->setRequestSuccess();
        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->getContent());
        // Asserts Database Images
        $project = Project::find(1);
        $this->assertStringContainsString('avatar.png', $project->image);
    }

    public function testUploadImageFilesExistSuccess()
    {
        $request = $this->setRequestSuccess();
        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->getContent());
        // Asserts Files Exists
        $directory = public_path('image') . DIRECTORY_SEPARATOR . 'projects/';
        $this->assertTrue(file_exists($directory . time() . '-' . 'original-' . 'avatar.png'));
        $this->assertTrue(file_exists($directory . time() . '-' . 'medium-' . 'avatar.png'));
    }

    public function testUploadImageFilesSize()
    {
        $request = $this->setRequestSuccess();
        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->getContent());
        // Asserts Files Sizes
        $directory = public_path('image') . DIRECTORY_SEPARATOR . 'projects/';
        $filename = time() . '-' . 'medium-' . 'avatar.png';
        $imgMediumSize = getimagesize($directory . $filename);
        $this->assertContains(339, $imgMediumSize);
    }

    /**
     * @return TestResponse
     */
    private function setRequestSuccess()
    {
        $this->initializedUser();
        $this->setCategory();

        $category = Category::find(1);
        $data = [
            'title' => 'test de title',
            'content' => 'test de content',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(3)->format('Y-m-d'),
            'image' => UploadedFile::fake()->image('avatar.png')
        ];
        $route = route('projects.store');
        return $this->post($route, $data);
    }

    private function setCategory()
    {
        factory(Category::class, 1)->create();
    }
}
