<?php

namespace Project;

use App\Category;
use App\Project;
use App\Skill;
use App\SkillProject;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Tests\Unit\admin\TestUnitUserCase;

class ProjectControllerTest extends TestUnitUserCase
{
    public function testCreateProjectErrorValidator() {
        $this->initializedUser();

        $data = [
            'title' => 'test',
            'link_web' => 'test',
            'content' => 'content',
            'conclusion' => 'test',
            'category_id' => 'category',
            'start_project' => 'lol',
            'end_project' => Carbon::now(),
            'back_end' => 'PHP',
            'front_end' => 'JS',
            'image' => 'test'
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('danger', 'An error has been occurred when to creating project !');
        $this->assertStringContainsString('admin/projects/create', $request->content());

        // Asserts Data Projects
        $count = Project::count();
        $project = Project::find(1);
        $this->assertEquals(0, $count);
        $this->assertNull($project);
    }

    public function testCreateProjectStartDateTooRecent() {
        $this->initializedUser();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(2);
        $data = [
            'title' => 'test title',
            'link_web' => 'https://www.php.net',
            'content' => 'test content',
            'conclusion' => 'test conclusion',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->addYears(2)->format('Y-m-d'),
            'end_project' => Carbon::now()->format('Y-m-d'),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('danger', 'The end project date don\'t must be more recent than the start project date !');
        $this->assertStringContainsString('admin/projects/create', $request->content());

        // Asserts Data Projects
        $count = Project::count();
        $project = Project::find(1);
        $this->assertEquals(0, $count);
        $this->assertNull($project);
    }

    public function testCreateProjectStartDateEndDateIdentical() {
        $this->initializedUser();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(2);
        $data = [
            'title' => 'test title',
            'link_web' => 'https://www.php.net',
            'content' => 'test content conclusion',
            'conclusion' => 'test conclusion',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->format('Y-m-d'),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('danger', 'The end project date don\'t must be identical than the start project date !');
        $this->assertStringContainsString('admin/projects/create', $request->content());

        // Asserts Data Projects
        $count = Project::count();
        $project = Project::find(1);
        $this->assertEquals(0, $count);
        $this->assertNull($project);
    }

    public function testCreateProjectWithCategory()
    {
        $this->initializedUser();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(2);
        $data = [
            'title' => 'test title',
            'link_web' => 'https://www.php.net',
            'content' => 'test content',
            'conclusion' => 'test conclusion',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d'),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->content());

        // Asserts Data Projects
        $count = Project::count();
        $project = Project::find(1);
        $this->assertEquals(1, $count);
        $this->assertNotNull($project);
        $this->assertEquals(2, $project->category_id);
    }

    public function testCreateProjectSuccess() {
        $this->initializedUser();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(2);
        $data = [
            'title' => 'test title',
            'link_web' => 'https://www.php.net',
            'content' => 'test content',
            'conclusion' => 'test conclusion',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d'),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $route = route('projects.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are created your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->content());

        // Asserts Data Projects
        $count = Project::count();
        $project = Project::find(1);
        $this->assertEquals(1, $count);
        $this->assertEquals('test title', $project->title);
        $this->assertEquals('test-title', $project->slug);
        $this->assertEquals('test content', $project->content);
        $this->assertEquals('test conclusion', $project->conclusion);
        $this->assertEquals(2, $project->category_id);
        $this->assertNotNull($project);
    }

    public function testUpdateProjectErrorValidator() {
        $this->initializedUser();
        $this->createProject();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(1);
        $data = [
            'title' => 'test',
            'link_web' => 'test',
            'content' => 'content',
            'conclusion' => 'test',
            'category_id' => $category->id,
            'start_project' => 'lol',
            'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d'),
            'back_end' => 'PHP',
            'front_end' => 'JS',
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $project = Project::find(1);
        $route = route('projects.update', [$project->id]);
        $request = $this->put($route, $data);

        // Asserts Request
        $request->assertSessionHas('danger', 'An error has been occurred when to edited project !');
        $this->assertStringContainsString('admin/projects/' . $project->id . '/edit', $request->content());

        // Asserts Data Projects
        $project = Project::find(1);
        $count = Project::count();
        $this->assertEquals(1, $count);
        $this->assertEquals('test title', $project->title);
        $this->assertEquals('test-title', $project->slug);
        $this->assertEquals('test content', $project->content);
        $this->assertEquals(2, $project->category_id);
        $this->assertNotNull($project);
    }

    public function testUpdateProjectSuccess() {
        $this->initializedUser();
        $this->createProject();
        $this->createCategories(2);
        $this->createSkill();

        $category = Category::find(1);
        $data = [
            'title' => 'test title 2',
            'link_web' => 'https://www.php.net',
            'content' => 'test content 2',
            'conclusion' => 'test conclusion 2',
            'category_id' => $category->id,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d'),
            'back_end' => ['PHP'],
            'front_end' => ['JS'],
            'image' => UploadedFile::fake()->image('avatar.jpg')
        ];
        $project = Project::find(1);
        $route = route('projects.update', [$project->id]);
        $request = $this->put($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are edited your project with success !');
        $this->assertStringContainsString('admin/dashboard', $request->content());

        // Asserts Data Projects
        $project = Project::find(1);
        $count = Project::count();
        $this->assertEquals(1, $count);
        $this->assertEquals('test title 2', $project->title);
        $this->assertEquals('test-title-2', $project->slug);
        $this->assertEquals('test content 2', $project->content);
        $this->assertEquals(1, $project->category_id);
        $this->assertEquals(Carbon::now()->format('Y-m-d H:i:s'), $project->updated_at);
        $this->assertNotNull($project);
    }

    public function testDeleteProjectSuccess()
    {
        $this->initializedUser();
        $this->createProject();
        $this->createCategories(2);
        $this->createSkill();

        $project = Project::find(1);
        $route = route('projects.destroy', [$project]);
        $request = $this->delete($route);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are deleted your project with success !');
        $this->assertStringContainsString('/admin/projects', $request->getContent());

        // Assert Data
        $project = Project::find(1);
        $this->assertNull($project);
    }

    /**
     * @return void
     */
    private function createProject(): void
    {
        Project::create([
            'title' => 'test title',
            'slug' => 'test-title',
            'content' => 'test content',
            'category_id' => 2,
            'start_project' => Carbon::now()->format('Y-m-d'),
            'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d')
        ]);
    }

    /**
     * @return void
     */
    private function createSkill(): void
    {
        for ($i = 0; $i < 2; $i++) {
            Skill::create([
                'name' => $i === 0 ? 'PHP' : 'JS',
                'link' => $i === 0 ? 'https://www.php.net' : 'https://developer.mozilla.org/fr/docs/Web/JavaScript',
                'percentage' => 80,
                'type' => 'language',
                'back_or_front' => $i === 0 ? 'back_end' : 'front_end'
            ]);
        }
    }

    /**
     * @param int $value
     * @return void
     */
    private function createCategories(int $value): void
    {
        factory(Category::class, $value)->create();
    }
}
