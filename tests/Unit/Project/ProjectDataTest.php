<?php

namespace Project;

use App\Project;
use Carbon\Carbon;
use Tests\Unit\admin\TestUnitUserCase;

class ProjectDataTest extends TestUnitUserCase
{

    /**
     * @var Project
     */
    private Project $project;

    /**
     * ProjectDataTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->project = new Project();
    }

    public function testDataProjectIsANumber()
    {
        $this->initializedUser();
        $this->createProject();

        $projects = Project::all();
        $data = $this->project->setCountProjectByYear($projects);

        $this->assertEquals(1, count($data));
        foreach ($data as $item) {
            $this->assertIsInt($item);
        }
    }

    public function testDataProject() {
        $this->initializedUser();
        $this->createProject();

        $projects = Project::all();
        $data = $this->project->setCountProjectByYear($projects);

        $this->assertEquals([2], $data);
    }

    public function testDataProjectsYearsIsAString() {
        $this->initializedUser();
        $this->createProject();

        $projects = Project::all();
        $data = $this->project->setYearsData($projects);

        $this->assertEquals(1, count($data));
        foreach ($data as $item) {
            $this->assertIsString($item);
        }
    }

    public function testDataProjectsYears() {
        $this->initializedUser();
        $this->createProject();

        $projects = Project::all();
        $data = $this->project->setYearsData($projects);
        $this->assertEquals(['2020'], $data);
    }

    /**
     * Create two project in the database !
     */
    private function createProject(): void
    {
        for ($i = 0; $i < 2; $i++) {
            Project::create([
                'title' => 'title test ' . ($i + 1),
                'slug' => 'title-test-' . ($i + 1),
                'content' => 'title content ' . ($i + 1),
                'start_project' => Carbon::now()->format('Y-m-d'),
                'end_project' => Carbon::now()->addMonths(2)->format('Y-m-d')
            ]);
        }
    }
}
