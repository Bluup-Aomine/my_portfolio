<?php

namespace Settings;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tests\Unit\admin\TestUnitUserCase;

class SettingAdminTest extends TestUnitUserCase
{

    public function testEditSettingErrorValidator()
    {
        $this->initializedUser();

        $data = [
            'username' => 'tes',
            'email' => 'test',
            'password' => 'test password',
            'new_password' => '',
            'new_password_confirmation' => ''
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'An error has been occurred when to edited yours settings !');
        $this->assertStringContainsString('admin/settings', $request->getContent());
    }

    public function testEditSettingPasswordFieldCheckUser()
    {
        $this->initializedUser();

        $data = [
            'username' => 'BluupA0mine',
            'email' => 'test@test.net',
            'password' => 'test password',
            'new_password' => '',
            'new_password_confirmation' => ''
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'The field password must equal to this user !');
        $this->assertStringContainsString('admin/settings', $request->getContent());
    }

    public function testEditSettingIfNewPasswordAndConfirmationFieldsAreDontEqual()
    {
        $this->initializedUser();

        $data = [
            'username' => 'BluupA0mine',
            'email' => 'test@test.net',
            'password' => 'test12345',
            'new_password' => 'test password',
            'new_password_confirmation' => 'test de password'
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'Yours passwords are don\'t equal !');
        $this->assertStringContainsString('admin/settings', $request->getContent());
    }

    public function testEditSettingSuccessWithNewPasswordEmpty()
    {
        $this->initializedUser();

        $data = [
            'username' => 'BluupA0mine',
            'email' => 'test@test.net',
            'password' => 'test12345',
            'new_password' => '',
            'new_password_confirmation' => ''
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are edited yours settings with success !');
        $this->assertStringContainsString('admin/settings', $request->getContent());

        // Asserts Data User
        $user = User::find(1);
        $checkPassword = Hash::check($data['password'], $user->password);
        $this->assertNotNull($user);
        $this->assertEquals($data['username'], $user->name);
        $this->assertEquals($data['email'], $user->email);
        $this->assertTrue($checkPassword);
    }

    public function testEditSettingSuccessWithoutNewPassword()
    {
        $this->initializedUser();

        $data = [
            'username' => 'BluupA0mine',
            'email' => 'test@test.net',
            'password' => 'test12345',
            'new_password' => null,
            'new_password_confirmation' => null
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are edited yours settings with success !');
        $this->assertStringContainsString('admin/settings', $request->getContent());

        // Asserts Data User
        $user = User::find(1);
        $checkPassword = Hash::check($data['password'], $user->password);
        $this->assertNotNull($user);
        $this->assertEquals($data['username'], $user->name);
        $this->assertEquals($data['email'], $user->email);
        $this->assertTrue($checkPassword);
    }

    public function testEditSettingSuccess()
    {
        $this->initializedUser();

        $data = [
            'username' => 'BluupA0mine',
            'email' => 'test@test.net',
            'password' => 'test12345',
            'new_password' => 'test de password',
            'new_password_confirmation' => 'test de password'
        ];
        $route = route('settings.edit');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'You are edited yours settings with success !');
        $this->assertStringContainsString('admin/settings', $request->getContent());

        // Asserts Data User
        $user = User::find(1);
        $checkPassword = Hash::check($data['new_password'], $user->password);
        $this->assertNotNull($user);
        $this->assertEquals($data['username'], $user->name);
        $this->assertEquals($data['email'], $user->email);
        $this->assertTrue($checkPassword);
        $this->assertNotEquals($data['password'], $user->password);
    }

}
