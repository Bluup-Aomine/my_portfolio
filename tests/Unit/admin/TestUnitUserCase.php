<?php

namespace Tests\Unit\admin;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class TestUnitUserCase extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');
        $this->artisan('db:seed');
    }

    /**
     * Initialized users for tests unit
     */
    public function initializedUser() {
        $this->setUser();
        $this->connectUser();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getUser(int $id) {
        return User::find($id) ?: null;
    }

    private function setUser() {
        $this->createUser();
        factory(User::class, 2)->create();
    }

    private function connectUser() {
        $user = $this->getUser(1);
        $this->actingAs($user);
    }

    private function createUser()
    {
        User::create([
            'name' => 'Bissboss23',
            'email' => 'vincentcapek@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('test12345'),
            'remember_token' => Str::random(10),
        ]);
    }
}
