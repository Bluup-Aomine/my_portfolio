<?php

namespace Tests\Unit\Skill;

use App\Skill;
use Tests\Unit\admin\TestUnitUserCase;

class SkillDataTest extends TestUnitUserCase
{

    /**
     * @var Skill
     */
    private Skill $skill;

    /**
     * SkillDataTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->skill = new Skill();
    }

    public function testSkillPercentageNewDataIsFloat() {
        $this->setSkills();

        $skills = Skill::all();
        $data = $this->skill->setDataSkillsPercentage($skills);

        $this->assertEquals(10, count($data));
        foreach ($data as $dt) {
            $this->assertIsFloat($dt);
        }
    }

    public function testSkillTechnoNewDataIsString() {
        $this->setSkills();

        $skills = Skill::all();
        $data = $this->skill->setDataSkillsTechno($skills);

        $this->assertEquals(10, count($data));
        foreach ($data as $dt) {
            $this->assertIsString($dt);
        }
    }

    public function testSkillTechnoNewData() {
        $this->createSkills();

        $skills = Skill::all();
        $data = $this->skill->setDataSkillsTechno($skills);

        $this->assertEquals(['JS', 'PHP'], $data);
    }

    public function testSkillPercentageNewData() {
        $this->createSkills();

        $skills = Skill::all();
        $data = $this->skill->setDataSkillsPercentage($skills);

        $this->assertEquals([50.0, 70.0], $data);
    }

    private function createSkills() {
        for ($i = 0; $i < 2; $i++) {
            Skill::create([
                'name' => $i === 0 ? 'JS' : 'PHP',
                'percentage' => $i === 0 ? 50 : 70
            ]);
        }
    }

    private function setSkills() {
        factory(Skill::class, 10)->create();
    }
}
