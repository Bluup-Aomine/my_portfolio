<?php

namespace Tests\Unit\Skill;

use App\Skill;
use Tests\Unit\admin\TestUnitUserCase;

class SkillControllerTest extends TestUnitUserCase
{

    public function testIndexSkillsGetMethod()
    {
        $route = route('skills.index');
        $request = $this->get($route);
        $request->assertStatus(200);
        $request->assertViewIs('admin.skills.index');
    }

    public function testCreateSkillGetMethod()
    {
        $route = route('skills.create');
        $request = $this->get($route);
        $request->assertStatus(200);
    }

    public function testCreateSkillErrorValidator()
    {
        $this->initializedUser();
        $data = [
            'name' => 'PHP',
            'link' => 'test',
            'percentage' => 'PHP',
            'type' => false,
            'back_or_front' => false
        ];
        $route = route('skills.store');
        $request = $this->post($route, $data);
        $request->assertSessionHas('danger', 'An error has been occurred when to creating skill !');
        $this->assertStringContainsString('admin/skills/create', $request->content());
    }

    public function testCreateSkillErrorBadFieldType()
    {
        $this->initializedUser();
        $data = [
            'name' => 'PHP',
            'link' => 'https://www.php.net',
            'percentage' => 90,
            'type' => 'test',
            'back_or_front' => 'back_end'
        ];
        $route = route('skills.store');
        $request = $this->post($route, $data);

        // Request Assert
        $request->assertSessionHas('danger', 'The field type must contain "techno" or "language" !');
        $this->assertStringContainsString('admin/skills/create', $request->content());

        // Assert Skills
        $skill = Skill::find(1);
        $this->assertNull($skill);
    }

    public function testCreateSkillErrorBadFieldBackOrFront()
    {
        $this->initializedUser();
        $data = [
            'name' => 'PHP',
            'link' => 'https://www.php.net',
            'percentage' => 90,
            'type' => 'techno',
            'back_or_front' => 'test'
        ];
        $route = route('skills.store');
        $request = $this->post($route, $data);

        // Request Assert
        $request->assertSessionHas('danger', 'The field back or front must contain "back_end" or "front_end" !');
        $this->assertStringContainsString('admin/skills/create', $request->content());

        // Assert Skills
        $skill = Skill::find(1);
        $this->assertNull($skill);
    }

    public function testCreateSkillSuccess()
    {
        $this->initializedUser();
        $data = [
            'name' => 'PHP',
            'link' => 'https://www.php.net',
            'percentage' => 90,
            'type' => 'techno',
            'back_or_front' => 'back_end'
        ];
        $route = route('skills.store');
        $request = $this->post($route, $data);

        // Assert Skill
        $count = Skill::count();
        $skill = Skill::find(1);
        $this->assertNotNull($skill);
        $this->assertEquals(1, $count);
        $this->assertEquals('PHP', $skill->name);
        $this->assertEquals('https://www.php.net', $skill->link);
        $this->assertEquals(90, $skill->percentage);
        $this->assertEquals('techno', $skill->type);
        $this->assertEquals('back_end', $skill->back_or_front);

        // Request Assert
        $request->assertSessionHas('success', 'You are created your skill with success !');
        $this->assertStringContainsString('admin/skills', $request->content());
    }

    public function testEditSkillGetMethod()
    {
        $this->createSkill();

        $skill = Skill::find(1);
        $route = route('skills.edit', [$skill]);
        $request = $this->get($route);
        $request->assertStatus(200);
        $request->assertViewIs('admin.skills.edit');
    }

    private function createSkill()
    {
        Skill::create([
            'name' => 'PHP',
            'percentage' => 90,
            'type' => 'language',
            'back_or_front' => 'back_end'
        ]);
    }

    public function testEditSkillErrorValidator()
    {
        $this->initializedUser();
        $this->createSkill();

        $data = [
            'name' => 'PHP',
            'link' => 'test',
            'percentage' => 'PHP',
            'type' => false,
            'back_or_front' => false
        ];
        $skill = Skill::find(1);
        $route = route('skills.update', $skill);
        $request = $this->put($route, $data);

        // Request Asserts
        $request->assertSessionHas('danger', 'An error has been occurred when to editing skill !');
        $this->assertStringContainsString('admin/skills/' . $skill->id . '/edit', $request->content());
    }

    public function testEditSkillSuccess()
    {
        $this->initializedUser();
        $this->createSkill();

        $data = [
            'name' => 'PHP',
            'link' => 'https://www.php.net',
            'percentage' => 80,
            'type' => 'techno',
            'back_or_front' => 'front_end'
        ];
        $skill = Skill::find(1);
        $route = route('skills.update', $skill);
        $request = $this->put($route, $data);

        // Assert Skill
        $count = Skill::count();
        $skillEdited = Skill::find(1);
        $this->assertNotNull($skillEdited);
        $this->assertEquals(1, $count);
        $this->assertEquals($data['name'], $skillEdited->name);
        $this->assertEquals($data['link'], $skillEdited->link);
        $this->assertEquals($data['percentage'], $skillEdited->percentage);
        $this->assertEquals($data['type'], $skillEdited->type);
        $this->assertEquals($data['back_or_front'], $skillEdited->back_or_front);

        // Request Assert
        $request->assertSessionHas('success', 'You are updated your skill with success !');
        $this->assertStringContainsString('admin/skills', $request->content());
    }
}
