<?php

namespace Tests\Unit\categories;

use App\Category;
use Tests\Unit\admin\TestUnitUserCase;

class CategoryControllerTest extends TestUnitUserCase
{
    public function testCreateCategoryValidatorError()
    {
        $this->initializedUser();

        $data = [
            'name' => 't',
            'content' => 'test'
        ];
        $route = route('categories.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('danger', 'An error has been occurred when to creating project !');
        $this->assertStringContainsString('admin/categories/create', $request->getContent());

        // Asserts Data Categories
        $category = Category::find(1);
        $count = Category::count();
        $this->assertEquals(0, $count);
        $this->assertNull($category);
    }

    public function testCreateCategorySlugValid()
    {
        $this->initializedUser();

        $data = [
            'name' => 'test de test',
            'content' => 'test content'
        ];
        $route = route('categories.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are created your category with success !');
        $this->assertStringContainsString('admin/categories', $request->getContent());

        // Asserts Data Categories
        $category = Category::find(1);
        $this->assertNotNull($category);
        $this->assertEquals('test de test', $category->name);
        $this->assertEquals('test-de-test', $category->slug);
    }

    public function testCreateCategorySuccess()
    {
        $this->initializedUser();

        $data = [
            'name' => 'test',
            'content' => 'test content'
        ];
        $route = route('categories.store');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are created your category with success !');
        $this->assertStringContainsString('admin/categories', $request->getContent());

        // Asserts Data Categories
        $category = Category::find(1);
        $count = Category::count();
        $this->assertEquals(1, $count);
        $this->assertNotNull($category);
        $this->assertEquals('test', $category->name);
        $this->assertEquals('test content', $category->content);
    }

    public function testUpdateCategoryValidatorError()
    {
        $this->initializedUser();
        $this->setCategory();

        $data = [
            'name' => 't',
            'content' => 'test'
        ];
        $route = route('categories.update', [1]);
        $request = $this->put($route, $data);

        // Asserts Request
        $category = Category::find(1);
        $request->assertSessionHas('danger', 'An error has been occurred when to editing project !');
        $this->assertStringContainsString('admin/categories/' . $category->id . '/edit', $request->getContent());

        // Asserts Data Categories
        $category = Category::find(1);
        $count = Category::count();
        $this->assertEquals(1, $count);
        $this->assertNotNull($category);
        $this->assertNotEquals('t', $category->name);
        $this->assertNotEquals('test', $category->content);
    }

    public function testUpdateCategorySuccess()
    {
        $this->initializedUser();
        $this->setCategory();

        $data = [
            'name' => 'test de test 2',
            'content' => 'test content 2'
        ];
        $route = route('categories.update', [1]);
        $request = $this->put($route, $data);

        // Asserts Request
        $request->assertSessionHas('success', 'You are edited your category with success !');
        $this->assertStringContainsString('admin/categories', $request->getContent());

        // Asserts Data Categories
        $category = Category::find(1);
        $count = Category::count();
        $this->assertEquals(1, $count);
        $this->assertNotNull($category);
        $this->assertEquals('test de test 2', $category->name);
        $this->assertEquals('test-de-test-2', $category->slug);
        $this->assertEquals('test content 2', $category->content);
    }

    /**
     * @return void
     */
    private function setCategory(): void
    {
        Category::create([
            'name' => 'test de test',
            'slug' => 'test-de-test',
            'content' => 'test de content'
        ]);
    }
}
