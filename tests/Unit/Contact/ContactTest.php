<?php

namespace Contact;

use App\Mail\Contact;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Mail;
use Tests\Unit\admin\TestUnitUserCase;

class ContactTest extends TestUnitUserCase
{

    use WithoutMiddleware;

    public function testContactErrorValidator()
    {
        $data = [
            'name' => 't',
            'email' => 'test',
            'subject' => 'te',
            'content' => 'test'
        ];
        $route = route('contact.send');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('danger', 'Vous avez rencontré une erreur lors de l\'envoie de l\'email !');
        $this->assertStringContainsString('/contact', $request->getContent());
    }

    public function testContactSuccess()
    {
        $data = [
            'name' => 'Monsieur Test',
            'email' => 'test@test.gmail',
            'subject' => 'Appel de vos services',
            'content' => 'Content pour vos services'
        ];
        $route = route('contact.send');
        $request = $this->post($route, $data);

        // Asserts Request
        $request->assertStatus(302);
        $request->assertSessionHas('success', 'Vous avez envoyé votre mail avec succès !');
    }

    public function testContactSendingMail()
    {
        $data = [
            'name' => 'Monsieur Test',
            'email' => 'test@test.gmail',
            'subject' => 'Appel de vos services',
            'content' => 'Content pour vos services'
        ];
        $route = route('contact.send');
        Mail::fake();

        $this->post($route, $data)->assertStatus(302);

        Mail::assertSent(Contact::class);
    }
}
