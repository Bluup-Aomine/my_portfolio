<?php

namespace App\src\HTML;

use App\src\Helpers\Image;

class Projects
{
    /**
     * @var Image
     */
    private Image $image;

    /**
     * Projects constructor.
     */
    public function __construct()
    {
        $this->image = new Image();
    }

    /**
     * @param array $projects
     * @return array|string[]
     */
    public function listProject(array $projects): array
    {
        return array_map(function (array $project) {
            return $this->setProject($project);
        }, $projects);
    }

    /**
     * @param array $project
     * @return mixed
     */
    private function setProject(array $project)
    {
        $img = $project['image'] === null ? asset('img/p1.jpg') : $this->image->thumbImage(asset('image/projects/' . $project['image']));
        $preview = asset('img/preview.png') ?: '';
        return '<div class="portfolio-single col-sm-4">
                            <div class="relative">
                                <div class="thumb">
                                    <div class="overlay overlay-bg"></div>
                                    <img src="' . $img . '" alt="project image" class="img-fluid">
                                </div>
                                <a href="#">
                                    <div class="middle">
                                        <div class="text align-self-center d-flex">
                                            <img src="' . $preview . '" alt="icon preview hover">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="p-inner">
                                <h4>' . $project['title'] . '</h4>
                                <div class="category">' . $project['category']['name'] . '</div>
                            </div>
                        </div>';
    }
}
