<?php

namespace App\src\Helpers;

class Text
{
    /**
     * @param string $content
     * @return false|string[]
     */
    public static function lineBreak(string $content): array
    {
        $contents = explode("\n", $content);
        return array_filter($contents, fn ($content) => $content !== "\r");
    }
}
