<?php

namespace App\src\Helpers;

class Image
{

    /**
     * @param string $underDir
     * @return string
     */
    public function getDirectory(string $underDir = ''): string
    {
        $directory = public_path('image');
        return $directory . DIRECTORY_SEPARATOR . $underDir;
    }

    /**
     * @param string $image
     * @return string|string[]
     */
    public function thumbImage(string $image) {
        return str_replace('original', 'medium', $image);
    }
}
