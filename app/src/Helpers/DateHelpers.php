<?php

namespace App\src\Helpers;

use Carbon\Carbon;

class DateHelpers
{
    /**
     * @param string|null $startDate
     * @param string|null $endDate
     * @return bool
     */
    public function tooRecentEndDate(?string $startDate, ?string $endDate): bool {
        [$startDate, $endDate] = $this->parsedStartAndEndDate($startDate, $endDate);
        return $endDate < $startDate;
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     * @return bool
     */
    public function identicalDate(?string $startDate, ?string $endDate): bool
    {
        [$startDate, $endDate] = $this->parsedStartAndEndDate($startDate, $endDate);
        return $startDate === $endDate;
    }

    /**
     * @param string|null $startDate
     * @param string|null $endDate
     * @return array
     */
    private function parsedStartAndEndDate(?string $startDate, ?string $endDate): array
    {
        $startDate = Carbon::parse($startDate)->toDateString();
        $endDate = Carbon::parse($endDate)->toDateString();

        return [$startDate, $endDate];
    }
}
