<?php

namespace App\src\Helpers;

class Regex
{
    /**
     * @return string
     */
    public static function Url(): string
    {
        return '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
    }
}
