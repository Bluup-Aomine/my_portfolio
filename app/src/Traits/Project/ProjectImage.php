<?php

namespace App\src\Traits\Project;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait ProjectImage
{
    /**
     * @var array|string[]
     */
    private array $extensions = ['JPG', 'PNG', 'JPEG'];
    /**
     * @var array|string[]
     */
    private array $sizes = [
        'original' => [],
        'medium' => ['width' => 340, 'height' => 339]
    ];
    /**
     * @var string
     */
    private string $fileOriginal = '';
    /**
     * @var bool
     */
    private bool $error = false;
    /**
     * @var string
     */
    private string $messageError = '';

    /**
     * @param Request $request
     * @return void|string
     */
    public function setImage(Request $request)
    {
        if ($request->file()) {;
            $file = $request->file('image');
            if (!$file || is_null($file)) {
                $this->messageError("The field image don't exist !");
                return;
            }
            $extension = mb_strtoupper($file->clientExtension());
            if (!in_array($extension, $this->extensions)) {
                $implode = implode(', ', $this->extensions);
                $this->messageError("You file must be to type ('$implode') !");
                return;
            }
            // Treatment Image
            $this->moveImage($file);
            return $this->getImage();
        }
        $this->messageError("The field image don't exist !");
    }

    /**
     * @param UploadedFile $file
     */
    private function moveImage(UploadedFile $file)
    {
        $directory = $this->imageBuild->getDirectory('projects/');
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        Image::configure(['driver' => 'imagick']);
        $time = time();
        foreach ($this->sizes as $key => $size) {
            $img = Image::make($file->getRealPath());
            if (!empty($size)) {
                $width = $size['width'];
                $height = $size['height'];
                $img->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            if ($key === 'original') {
                $this->fileOriginal = $time . '-' . $key . '-' . $file->getClientOriginalName();
            }
            $filename = $directory . $time . '-' . $key . '-' . $file->getClientOriginalName();
            $img->save($filename);
        }
    }

    /**
     * @return string
     */
    private function getImage()
    {
        return $this->fileOriginal;
    }

    /**
     * @return bool
     */
    private function error(): bool
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    private function messageError(string $error): void
    {
        $this->error = true;
        $this->messageError = $error;
    }

    /**
     * @param bool $create
     * @param \App\Project|null $project
     * @return RedirectResponse
     */
    private function setError(bool $create = true, ?\App\Project $project = null): RedirectResponse
    {
        $route = $create ? 'projects.create' : 'project.update';
        $params = $create ? [] : [$project];
        return $this->message->renderError($route, $params, $this->messageError);
    }
}
