<?php

namespace App\src\Traits\Project;

use App\src\Helpers\Text;

trait Project
{
    /**
     * @return false|int|string
     */
    public function getContent()
    {
        $value = $this->content;
        $length = mb_strlen($value);
        return $length > 100 ? mb_substr($value, 0, 100) . '...' : $value;
    }

    /**
     * @return false|string[]
     */
    public function getContents()
    {
        $content = $this->content;
        return Text::lineBreak($content);
    }

    /**
     * @return false|string[]
     */
    public function getConclusions()
    {
        $conclusion = $this->conclusion;
        return Text::lineBreak($conclusion);
    }
}
