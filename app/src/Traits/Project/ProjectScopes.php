<?php

namespace App\src\Traits\Project\Project;

trait ProjectScopes
{
    /**
     * @param $query
     * @param string $slug
     * @return mixed
     */
    public function scopeGetBySlug($query, string $slug)
    {
        return $query->where('slug', $slug);
    }

    /**
     * @param $query
     * @param int $categoryID
     * @return mixed
     */
    public function scopeGetByCategory($query, int $categoryID)
    {
        return $query->with('category')->where('category_id', $categoryID);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLastQuery($query)
    {
        return $query->orderBy('id', 'desc');
    }
}
