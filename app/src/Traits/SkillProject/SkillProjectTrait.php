<?php

namespace App\src\Traits\SkillProject;

use App\Skill;

trait SkillProjectTrait
{
    /**
     * @param string $name
     * @return mixed
     */
    public function getSkill(string $name)
    {
        return Skill::GetByName($name)->first();
    }

    /**
     * @param $query
     * @param string $whereQuery
     * @param int $projectID
     * @return mixed
     */
    public function scopeGetByProjectID($query, string $whereQuery, int $projectID)
    {
        return $query->where($whereQuery, $projectID);
    }

    /**
     * @param $query
     * @param string $whereQuery
     * @param string $backOrFront
     * @return mixed
     */
    public function scopeGetByBackOrFront($query, string $whereQuery, string $backOrFront)
    {
        return $query->where($whereQuery, $backOrFront);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJoinSkills($query)
    {
        return $query->leftJoin('skills', 'project_skill_project.skill_id', '=', 'skills.id');
    }
}
