<?php

namespace App\src\Traits;

use App\src\Messages\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

trait SkillControllerTrait
{
    /**
     * @var Message
     */
    private Message $message;

    /**
     * SkillControllerTrait constructor.
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @param array $credentials
     * @return bool
     */
    public function checkDataType(array $credentials): bool
    {
        $type = $credentials['type'] ?: null;
        return $type !== 'language' && $type !== 'techno';
    }

    /**
     * @param array $credentials
     * @return bool
     */
    public function checkDataBackOrFront(array $credentials): bool
    {
        $backOrFront = $credentials['back_or_front'] ?: null;
        return $backOrFront !== 'back_end' && $backOrFront !== 'front_end';
    }

    /**
     * @param string $message
     * @return RedirectResponse
     */
    public function setError(string $message): RedirectResponse
    {
        return $this->message->renderError('skills.create', [], $message);
    }

}
