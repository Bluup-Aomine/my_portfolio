<?php

namespace App\src\Messages;

use Illuminate\Http\JsonResponse;

class MessageJSON
{
    /**
     * @param string $message
     * @return JsonResponse
     */
    public function messageError(string $message): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], 302);
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    public function messageData(array $data): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => $data
        ], 302);
    }
}
