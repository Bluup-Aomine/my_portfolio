<?php

namespace App\src\Messages;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\RedirectResponse;

class Message
{
    /**
     * @param string $route
     * @param array $params
     * @param Validator|null $validator
     * @param string $message
     * @return RedirectResponse
     */
    public function renderError(string $route, array $params = [], string $message, ?Validator $validator = null): RedirectResponse
    {
        $redirect = redirect()->route($route, $params, 302);
        if ($validator) {
            $redirect->withErrors($validator->errors());
        }
        return $redirect->with('danger', $message);
    }

    /**
     * @param string $route
     * @param array $params
     * @param string $message
     * @return RedirectResponse
     */
    public function renderSuccess(string $route, array $params, string $message): RedirectResponse
    {
        return redirect()->route($route, $params)->with('success', $message);
    }
}
