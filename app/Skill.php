<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    public bool $timestamps = true;
    /**
     * @var array|string[]
     */
    protected array $fillable = ['name', 'link', 'percentage', 'type', 'back_or_front'];

    /**
     * @param string $value
     * @return string
     */
    public function getCreatedAtAttribute(string $value): string
    {
        return Carbon::create($value)->format('Y-m-d');
    }

    /**
     * @param object $skills
     * @return array
     */
    public function setDataSkillsPercentage(object $skills): array
    {
        $newData = [];
        foreach ($skills as $skill) {
            $newData[] = floatval($skill->percentage);
        }
        return $newData;
    }

    /**
     * @param object $skills
     * @return array
     */
    public function setDataSkillsTechno(object $skills): array
    {
        $newData = [];
        foreach ($skills as $skill) {
            $newData[] = $skill->name ?: 'Techno Undefined';
        }
        return $newData;
    }

    /**
     * @param $query
     * @param string $type
     * @return mixed
     */
    public function scopeByType($query, string $type)
    {
        return $query->where('type', $type);
    }

    /**
     * @param $query
     * @param string $backOrFront
     * @return mixed
     */
    public function scopeBackOrFront($query, string $backOrFront = 'back_end')
    {
        return $query->where('back_or_front', $backOrFront);
    }

    /**
     * @param $query
     * @param string $name
     * @return mixed
     */
    public function scopeGetByName($query, string $name)
    {
        return $query->where('name', $name);
    }
}
