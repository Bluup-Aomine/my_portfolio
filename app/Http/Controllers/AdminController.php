<?php

namespace App\Http\Controllers;

use App\Project;
use App\Skill;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Application|Factory|View
     */
    public function dashboard() {
        $skills = Skill::paginate(10);
        $projects = Project::paginate(10);

        $countProjects = Project::count();
        $countSkills = Skill::count();
        return view('admin.dashboard', compact('skills', 'projects', 'countProjects', 'countSkills'));
    }
}
