<?php

namespace App\Http\Controllers;

use App\Skill;
use App\src\Helpers\Regex;
use App\src\Traits\SkillControllerTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class SkillController extends Controller
{

    use SkillControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $skills = Skill::paginate(10);
        return view('admin.skills.index', compact('skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $skill = null;
        return view('admin.skills.create', compact('skill'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $credentials = $request->all();
        $validator = $this->setValidator($credentials);
        if ($validator->fails()) {
            return $this->message->renderError('skills.create', [], 'An error has been occurred when to creating skill !', $validator);
        }
        if ($this->checkDataType($credentials)) {
            return $this->setError('The field type must contain "techno" or "language" !');
        }
        if ($this->checkDataBackOrFront($credentials)) {
            return $this->setError('The field back or front must contain "back_end" or "front_end" !');
        }
        Skill::create($credentials);
        return redirect()->route('skills.index')->with('success', 'You are created your skill with success !');
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function setValidator(array $credentials): \Illuminate\Contracts\Validation\Validator
    {
        $regex = Regex::Url();
        return Validator::make($credentials, [
            'name' => 'required|max:25',
            'link' => 'required|regex:' . $regex,
            'percentage' => 'required|integer',
            'type' => 'required|string',
            'back_or_front' => 'required|string'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Skill $skill
     * @return Response
     */
    public function show(Skill $skill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Skill $skill
     * @return Application|Factory|View
     */
    public function edit(Skill $skill)
    {
        $skill = Skill::find((integer)$skill->id);
        return view('admin.skills.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Skill $skill
     * @return RedirectResponse
     */
    public function update(Request $request, Skill $skill): RedirectResponse
    {
        $credentials = $request->all();
        $validator = $this->setValidator($credentials);
        if ($validator->fails()) {
            return redirect()
                ->route('skills.edit', [$skill])
                ->withErrors($validator->errors())
                ->with('danger', 'An error has been occurred when to editing skill !');
        }
        if ($this->checkDataType($credentials)) {
            return $this->setError('The field type must contain "techno" or "language" !');
        }
        if ($this->checkDataBackOrFront($credentials)) {
            return $this->setError('The field back or front must contain "back_end" or "front_end" !');
        }
        $skill->update($credentials);
        return redirect()->route('skills.index')->with('success', 'You are updated your skill with success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Skill $skill
     * @return RedirectResponse
     */
    public function destroy(Skill $skill): RedirectResponse
    {
        $skill = Skill::findOrFail($skill->id);
        $skill->delete();
        return $this->message->renderSuccess('skills.index', [], 'You are deleted your skill with success !');
    }
}
