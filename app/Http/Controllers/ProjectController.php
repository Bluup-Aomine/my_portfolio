<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use App\Skill;
use App\SkillProject;
use App\src\Helpers\DateHelpers;
use App\src\Helpers\Image;
use App\src\Helpers\Regex;
use App\src\Messages\Message;
use App\src\Traits\Project\ProjectImage;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

class ProjectController extends Controller
{

    use ProjectImage;

    /**
     * @var DateHelpers
     */
    private DateHelpers $dateHelper;
    /**
     * @var Message
     */
    private Message $message;
    /**
     * @var Image
     */
    private Image $imageBuild;
    /**
     * @var SkillProject
     */
    private SkillProject $skillProject;

    /**
     * ProjectController constructor.
     */
    public function __construct() {
        $this->dateHelper = new DateHelpers();
        $this->message = new Message();
        $this->imageBuild = new Image();
        $this->skillProject = new SkillProject();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $projects = Project::paginate(10);
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        $project = null;
        $categories = Category::all();

        $skillsBack = Skill::BackOrFront()->get();
        $skillsFront = Skill::BackOrFront('front_end')->get();
        return view('admin.projects.create', compact('project', 'categories', 'skillsBack', 'skillsFront'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $credentials = $request->except('back_end', 'front_end');
        $validator = $this->setValidator($request->all());
        if ($validator->fails()) {
            return redirect()
                ->route('projects.create')
                ->withErrors($validator->errors())
                ->with('danger', 'An error has been occurred when to creating project !');
        }
        // Validate data request
        // Verify If end date project is more recent || start date project and end fate project are identical
        if ($this->dateHelper->tooRecentEndDate($credentials['start_project'], $credentials['end_project'])) {
            return redirect()
                ->route('projects.create')
                ->with('danger', 'The end project date don\'t must be more recent than the start project date !');
        }
        if ($this->dateHelper->identicalDate($credentials['start_project'], $credentials['end_project'])) {
            return redirect()
                ->route('projects.create')
                ->with('danger', 'The end project date don\'t must be identical than the start project date !');
        }
        $image = $this->setImage($request);
        if ($this->error()) {
            return $this->setError();
        }
        $credentials['slug'] = Str::slug($credentials['title']);
        $credentials['image'] = $image;
        Project::create($credentials);

        $credentials = $request->only('back_end', 'front_end');
        $this->skillProject->setSkillsToProject($credentials);
        if ($this->skillProject->error()) {
            return $this->skillProject->setError();
        }
        return redirect()->route('admin.dashboard')->with('success', 'You are created your project with success !');
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Application|Factory|Response|View
     */
    public function show(Project $project)
    {
        $skillsBackEnd = SkillProject::joinSkills()
            ->GetByProjectID('project_skill_project.project_id', $project->id)
            ->GetByBackOrFront('skills.back_or_front', 'back_end')
            ->get();
        $skillsFrontEnd = SkillProject::joinSkills()
            ->GetByProjectID('project_skill_project.project_id', $project->id)
            ->GetByBackOrFront('skills.back_or_front', 'front_end')
            ->get();
        $project = Project::findOrFail($project->id);
        return \view('projects.show', compact('project', 'skillsBackEnd', 'skillsFrontEnd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Application|Factory|Response|View
     */
    public function edit(Project $project)
    {
        $project = Project::findOrfail($project->id);
        $categories = Category::all();

        $skillsBack = Skill::BackOrFront()->get();
        $skillsFront = Skill::BackOrFront('front_end')->get();
        return view('admin.projects.edit', compact('project', 'categories', 'skillsBack', 'skillsFront'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Project $project
     * @return RedirectResponse
     */
    public function update(Request $request, Project $project): RedirectResponse
    {
        $credentials = $request->except('back_end', 'front_end');
        $validator = $this->setValidator($request->all());
        if ($validator->fails()) {
            return redirect()
                ->route('projects.edit', [$project->id])
                ->withErrors($validator->errors())
                ->with('danger', 'An error has been occurred when to edited project !');
        }
        // Validate data request
        // Verify If end date project is more recent || start date project and end fate project are identical
        if ($this->dateHelper->tooRecentEndDate($credentials['start_project'], $credentials['end_project'])) {
            return redirect()
                ->route('projects.create')
                ->with('danger', 'The end project date don\'t must be more recent than the start project date !');
        }
        if ($this->dateHelper->identicalDate($credentials['start_project'], $credentials['end_project'])) {
            return redirect()
                ->route('projects.create')
                ->with('danger', 'The end project date don\'t must be identical than the start project date !');
        }
        $credentials['slug'] = Str::slug($credentials['title']);
        $project->update($credentials);

        $credentials = $request->only('back_end', 'front_end');
        $this->skillProject->setSkillsToProject($credentials);
        if ($this->skillProject->error()) {
            return $this->skillProject->setError();
        }
        return redirect()->route('admin.dashboard')->with('success', 'You are edited your project with success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project|null $project
     * @return RedirectResponse
     */
    public function destroy(?Project $project): RedirectResponse
    {
        $project = Project::findOrFail($project->id);
        $project->delete();
        return $this->message->renderSuccess('projects.index', [], 'You are deleted your project with success !');
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function setValidator(array $credentials): \Illuminate\Contracts\Validation\Validator
    {
        $regex =  Regex::Url();
        return Validator::make($credentials, [
            'title' => 'required|min:5|max:250',
            'link_web' => 'required|regex:' . $regex,
            'content' => 'required|min:10',
            'conclusion' => 'required|min:10',
            'category_id' => 'required|integer',
            'start_project' => 'required|date',
            'end_project' => 'required|date',
            'back_end' => 'required|array|min:1',
            'front_end' => 'required|array|min:1',
            'front_end.*' => 'required|string|distinct|min:2',
            'back_end.*' => 'required|string|distinct|min:2',
            'image' => 'file'
        ]);
    }
}
