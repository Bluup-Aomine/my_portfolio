<?php

namespace App\Http\Controllers;

use App\src\Messages\Message;
use App\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class SettingController extends Controller
{
    /**
     * @var Message
     */
    private Message $message;

    /**
     * SettingController constructor.
     * @param Message $message
     */
    public function __construct(Message $message)
    {

        $this->message = $message;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $user = $this->getUserModel();
        return view('admin.settings.index', compact('user'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function edit(Request $request): RedirectResponse
    {
        $credentials = $request->all();
        $validator = $this->setValidator($credentials);
        if ($validator->fails()) {
            return $this->message->renderError('settings.index', [], 'An error has been occurred when to edited yours settings !', $validator);
        }
        if (!$this->checkPasswordUser($credentials['password'])) {
            return $this->message->renderError('settings.index', [], 'The field password must equal to this user !');
        }
        $data = $this->setValueDefaultNewPassword($credentials);
        return $this->updateUser($data);
    }

    /**
     * @param array $data
     * @return RedirectResponse
     */
    private function updateUser(array $data): RedirectResponse
    {
        [$newPassword, $newPasswordConfirmation, $bool] = $this->setDataNewPassword($data);
        $data['password'] = Hash::make($data['password']);
        if ($bool) {
            if ($newPassword !== $newPasswordConfirmation) {
                return $this->message->renderError('settings.index', [], 'Yours passwords are don\'t equal !');
            }
            $data['password'] = Hash::make($newPassword);
        }
        $data['name'] = $data['username'];
        unset($data['username'], $data['new_password'], $data['new_password_confirmation']);

        $this->getUserModel()->update($data);
        return $this->message->renderSuccess('settings.index', [], 'You are edited yours settings with success !');
    }

    /**
     * @param array $data
     * @return array
     */
    private function setDataNewPassword(array $data): array
    {
        $newPassword = $data['new_password'] ?: null;
        $newPasswordConfirmation = $data['new_password_confirmation'] ?: null;
        $bool = (!empty($newPassword) && !empty($newPasswordConfirmation));

        return [$newPassword, $newPasswordConfirmation, $bool];
    }

    /**
     * @param array $data
     * @return array
     */
    private function setValueDefaultNewPassword(array $data): array
    {
        if (is_null($data['new_password']) && is_null($data['new_password_confirmation'])) {
            $data['new_password'] = '';
            $data['new_password_confirmation'] = '';
        }
        return $data;
    }

    /**
     * @param string $passwordField
     * @return bool
     */
    private function checkPasswordUser(string $passwordField): bool
    {
        return Hash::check($passwordField, $this->getUserModel()->password);
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function setValidator(array $credentials)
    {
        return Validator::make($credentials, [
            'username' => 'required|min:4|max:60|string',
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
    }

    /**
     * @return User
     */
    private function getUserModel(): User
    {
        return User::find($this->getUser()->id);
    }

    /**
     * @return Authenticatable|null
     */
    private function getUser()
    {
        return Auth::user();
    }

}
