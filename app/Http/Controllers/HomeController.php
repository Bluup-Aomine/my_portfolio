<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use App\Skill;
use App\src\Helpers\Image;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index() {
        $projects = Project::with('category')->paginate(6);
        $categories = Category::all();

        $image = new Image();
        return view('index', compact('projects', 'categories', 'image'));
    }

    /**
     * @return Application|Factory|View
     */
    public function about() {
        $skillsLanguage = Skill::byType('language')->get();
        $skillsTechno = Skill::byType('techno')->get();
        return view('pages.about-page', compact('skillsLanguage', 'skillsTechno'));
    }

    /**
     * @return Application|Factory|View
     */
    public function services() {
        return view('pages.services');
    }

    /**
     * @return Application|Factory|View
     */
    public function portfolio() {
        $projects = Project::with('category')->paginate(6);
        $categories = Category::all();

        $image = new Image();
        return view('pages.portfolio', compact('projects', 'categories', 'image'));
    }
}
