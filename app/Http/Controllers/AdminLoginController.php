<?php

namespace App\Http\Controllers;

use App\src\Messages\Message;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class AdminLoginController extends Controller
{
    /**
     * @var Message
     */
    private Message $message;

    /**
     * AdminLoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->only('admin', 'login');
        $this->message = new Message();
    }

    /**
     * @return Application|Factory|View
     */
    public function admin() {
        return view('admin.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function login(Request $request): RedirectResponse {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->route('admin.dashboard')->with('success', 'Vous êtes maintenant connecté !');
        }
        return redirect()->back()->with('error', 'Vos identifiants sont incorrects !');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return $this->message->renderSuccess('admin', [], 'Vous vous êtes déconnecté avec succès !');
    }
}
