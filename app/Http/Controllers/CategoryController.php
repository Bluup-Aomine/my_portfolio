<?php

namespace App\Http\Controllers;

use App\Category;
use App\src\Messages\Message;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;

class CategoryController extends Controller
{

    /**
     * @var Message
     */
    private Message $message;

    public function __construct()
    {
        $this->message = new Message();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $categories = Category::paginate(10);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        $category = null;
        return view('admin.categories.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $credentials = $request->all();
        $validator = Validator::make($credentials, [
            'name' => 'required|min:2|max:60',
            'content' => 'required|min:10'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('categories.create')
                ->withErrors($validator->errors())
                ->with('danger', 'An error has been occurred when to creating project !');
        }
        $credentials['slug'] = Str::slug($credentials['name']);
        Category::create($credentials);
        return redirect()->route('categories.index')->with('success', 'You are created your category with success !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|Response|View
     */
    public function edit(int $id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $credentials = $request->all();
        $validator = $this->setValidator($credentials);
        if ($validator->fails()) {
            return redirect()
                ->route('categories.edit', [$id])
                ->withErrors($validator->errors())
                ->with('danger', 'An error has been occurred when to editing project !');
        }
        $credentials['slug'] = Str::slug($credentials['name']);
        $category = Category::findOrFail($id);
        $category->update($credentials);
        return redirect()->route('categories.index')->with('success', 'You are edited your category with success !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return $this->message->renderSuccess('categories.index', [], 'You are deleted your category with success !');
    }

    /**
     * @param array $credentials
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function setValidator(array $credentials): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($credentials, [
            'name' => 'required|min:2|max:60',
            'content' => 'required|min:10'
        ]);
    }
}
