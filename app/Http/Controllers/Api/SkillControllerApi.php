<?php

namespace App\Http\Controllers\Api;

use App\Skill;
use Illuminate\Http\JsonResponse;

class SkillControllerApi
{
    /**
     * @var Skill
     */
    private Skill $skill;

    /**
     * SkillControllerApi constructor.
     */
    public function __construct() {
        $this->skill = new Skill();
    }

    /**
     * @return JsonResponse
     */
    public function setDataSkills(): JsonResponse
    {
        $skills = Skill::all();
        $skillsPercentage = $this->skill->setDataSkillsPercentage($skills);
        $skillsTechno = $this->skill->setDataSkillsTechno($skills);

        return response()->json([
            'success' => true,
            'skillsPercentage' => $skillsPercentage,
            'skillsTechno' => $skillsTechno
        ]);
    }
}
