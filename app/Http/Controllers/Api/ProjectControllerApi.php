<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Project;
use App\src\HTML\Projects;
use App\src\Messages\MessageJSON;
use Illuminate\Http\JsonResponse;

class ProjectControllerApi
{
    /**
     * @var Project
     */
    private Project $project;
    /**
     * @var MessageJSON
     */
    private MessageJSON $messageJSON;
    /**
     * @var Projects
     */
    private Projects $projectHTML;

    /**
     * ProjectControllerApi constructor.
     */
    public function __construct()
    {
        $this->project = new Project();
        $this->messageJSON = new MessageJSON();
        $this->projectHTML = new Projects();
    }

    /**
     * @return JsonResponse
     */
    public function setDataProjects(): JsonResponse
    {
        $projects = Project::all();
        $projectsYears = $this->project->setYearsData($projects);
        $projectsCount = $this->project->setCountProjectByYear($projects);

        return response()->json([
            'success' => true,
            'projectsYears' => $projectsYears,
            'projectsCount' => $projectsCount
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function filterAll(): JsonResponse
    {
        $projects = Project::with('category')->get()->toArray();
        if (empty($projects)) {
            return $this->messageJSON->messageError('Any projects has been found !');
        }
        return $this->messageJSON->messageData($this->projectHTML->listProject($projects));
    }

    /**
     * @param string $slug
     * @return JsonResponse
     */
    public function filter(string $slug): JsonResponse
    {
        if (empty($slug)) {
            return $this->messageJSON->messageError('The params slugFilter don\'t must be empty !');
        }
        $category = Category::GetBySlug($slug)->first() ?: null;
        if (is_null($category)) {
            return $this->messageJSON->messageError('This category don\'t exist in our database !');
        }
        $projects = Project::GetByCategory($category->id)->get()->toArray() ?: [];
        if (empty($projects)) {
            return $this->messageJSON->messageError('Any projects has been found with this category !');
        }
        return $this->messageJSON->messageData($this->projectHTML->listProject($projects));
    }
}
