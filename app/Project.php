<?php

namespace App;

use App\src\Traits\Project\Project as ProjectTrait;
use App\src\Traits\Project\Project\ProjectScopes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Project extends Model
{
    use ProjectTrait, ProjectScopes;

    /**
     * @var bool
     */
    public bool $timestamps = true;
    /**
     * @var array
     */
    protected array $guarded = [];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @param object $projects
     * @return array
     */
    public function setCountProjectByYear(object $projects): array
    {
        $dataProjects = [];
        foreach ($projects as $project) {
            $date = Carbon::parse($project->start_project)->format('Y');
            $dataProjects[$date][] = $project;
        }
        // Count the projects by Years !
        $data = [];
        foreach ($dataProjects as $dataProject) {
            $data[] = count($dataProject);
        }
        return $data;
    }

    /**
     * @param object $projects
     * @return array
     */
    public function setYearsData(object $projects): array
    {
        $dataYears = [];
        foreach ($projects as $project) {
            $year = Carbon::parse($project->start_project)->format('Y');
            if (!in_array($year, $dataYears)) {
                $dataYears[] = $year;
            }
        }
        return $dataYears;
    }
}
