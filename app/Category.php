<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    /**
     * @var array
     */
    protected array $guarded = [];

    /**
     * @var bool
     */
    public bool $timestamps = true;

    /**
     * @return HasMany
     */
    public function project(): HasMany
    {
        return $this->hasMany(Project::class);
    }

    /**
     * @param $query
     * @param string $slug
     * @return mixed
     */
    public function scopeGetBySlug($query, string $slug)
    {
        return $query->where('slug', $slug)->get();
    }
}
