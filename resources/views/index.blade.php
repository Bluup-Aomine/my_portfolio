@extends('layouts.app')

@section('content')
    <!-- COMPONENTS HERO -->
    <section class="hero">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 hero-left">
                    <h6>This is me</h6>
                    <h1>Bluup A0mine</h1>
                    <p>
                        You will begin to realise why this exercise is called the Dickens Pattern with reference to the
                        ghost showing Scrooge some different futures.
                    </p>
                    <a href="#" class="primary-btn text-uppercase">discover now</a>
                </div>
                <div class="col-l-6 col-md-6 hero-right d-flex align-self-end">
                    <img src="{{ asset('img/hero-img.png') }}" alt="image photo hero" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <!-- COMPONENTS ABOUT ME -->
    <section class="about-me pt-120">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 about-me-left">
                    <img src="{{ asset('img/about-img.png') }}" alt="about me image" class="img-fluid">
                </div>
                <div class="col-lg-5 col-md-6 about-me-right">
                    <h6>About Me</h6>
                    <h1 class="text-uppercase">Personal Details</h1>
                    <p>
                        Here, I focus on a range of items and features that we use in life without giving them a second
                        thought. such as Coca Cola. Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco.
                    </p>
                    <a href="#" class="primary-btn text-uppercase">View Full Details</a>
                </div>
            </div>
        </div>
    </section>
    <!-- COMPONENT SERVICES -->
    <section class="services services-me">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content col-lg-7">
                    <div class="title text-center">
                        <h1 class="mb-10">My Offered Services</h1>
                        <p>
                            At about this time of year, some months after New Year’s resolutions have been made and kept, or
                            made and neglected.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-pie-chart"></span>
                        <a href="#">
                            <h4>Web Design</h4>
                        </a>
                        <p>
                            “It is not because things are difficult that we do not dare; it is because we do not dare that
                            they are difficult.”
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-laptop-phone"></span>
                        <a href="#">
                            <h4>Web Development</h4>
                        </a>
                        <p>
                            If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-tablet"></span>
                        <a href="#">
                            <h4>App Development</h4>
                        </a>
                        <p>
                            Do you sometimes have the feeling that you’re running into the same obstacles over and over again? Many of my conflicts.                    </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--- COMPONENTS PROJECTS -->
    @include('component.filtersPortfolio')
@endsection

@section('extra-js')
<script src="{{ asset('js/front/Project/FilterProjects.js') }}" type="module" defer></script>
<script src="{{ asset('js/front/Project/LinkTabsProjects.js') }}" type="module" defer></script>
@endsection
