@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">Project "{{ $project->title }}"</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('projects.show', [$project]) }}">Project "{{ $project->title }}"</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="page-project">
        <div class="container">
            <div class="section-top-border">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="mb-30">Project Content</h3>
                    </div>
                    <div class="col-md-6 text-right">
                        <a target="_blank" href="{{ $project->link_web }}" class="generic-btn primary-border circle arrow">
                            Voir site <span class="lnr lnr-arrow-right"></span>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-4 mt-2">
                        <img class="img-fluid" src="{{ asset('image/projects/' . $project->image) }}"
                             alt="image project '{{ $project->title }}'">
                    </div>
                    <div class="col-md-8 mt-sm-20 left-align-p">
                        @foreach($project->getContents() as $content)
                            <p>{{ $content }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="section-top-border">
                <h3 class="mb-30">Conclusion</h3>
                <div class="row">
                    <div class="col-md-12 mt-sm-20 left-align-p">
                        @foreach($project->getConclusions() as $conclusion)
                            <p>{{ $conclusion }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="section-top-border">
                <h3 class="mb-30">Information</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block-title">
                            <h4 class="mb-20">Technologie Back End</h4>
                            <ul class="project-info">
                                @foreach($skillsBackEnd as $skillBackEnd)
                                    <li>
                                        <a target="_blank" href="{{ $skillBackEnd->link }}">{{ $skillBackEnd->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block-title">
                            <h4 class="mb-20">Technologie Front End</h4>
                            <ul class="project-info">
                                @foreach($skillsFrontEnd as $skillFrontEnd)
                                    <li>
                                        <a target="_blank" href="{{ $skillFrontEnd->link }}">{{ $skillFrontEnd->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
