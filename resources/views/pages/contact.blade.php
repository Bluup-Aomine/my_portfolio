@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">Contact</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('contact') }}">Contact</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 d-flex flex-column address-contact">
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-envelope"></span>
                        </div>
                        <div class="contact-details">
                            <h5>vincentcapek@gmail.com</h5>
                            <p>Send us your query anytime!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <form action="{{ route('contact.send') }}" method="POST" id="form-contact" class="contact-form text-right">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <input name="name" type="text" class="contact-input mb-20 form-control" placeholder="Entre votre nom..." required>
                                <input name="email" type="email" class="contact-input mb-20 form-control" placeholder="Entre votre mail..." required>
                                <input name="subject" type="text" class="contact-input mb-20 form-control" placeholder="Entre votre sujet..." required>
                            </div>
                            <div class="col-lg-6 form-group">
                                <textarea class="form-control" name="content" placeholder="Entrer un message..." required></textarea>
                            </div>
                            <div class="col-lg-12">
                                <button class="generic-btn primary">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
