@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">About Me</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('about') }}">About</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="about-me">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 about-me-left">
                    <img src="{{ asset('img/about-img.png') }}" alt="about me image" class="img-fluid">
                </div>
                <div class="col-lg-5 col-md-6 about-me-right">
                    <h6>About Me</h6>
                    <h1 class="text-uppercase">Personal Details</h1>
                    <p>
                        Here, I focus on a range of items and features that we use in life without giving them a second
                        thought. such as Coca Cola. Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco.
                    </p>
                    <a href="#" class="primary-btn text-uppercase">View Full Details</a>
                </div>
                <div class="col-lg-12 pt-60">
                    <p>
                        It won’t be a bigger problem to find one video game lover in your neighbor. Since the
                        introduction of Virtual Game, it has been achieving great heights so far as its popularity and
                        technological advancement are concerned. The history of video game is as interesting as a fairy
                        tale.
                    </p>
                    <p>
                        The quality of today’s video game was not at all there when video game first conceptualized and
                        played ever. During the formulative years, video games were created by using various interactive
                        electronic devices with various display formats. The first ever video game was designed in 1947
                        by Thomas T. Goldsmith Jr.
                    </p>
                    <h4 class="pt-30">Tools Expertness</h4>
                </div>
            </div>
            <div class="row skill-techno">
                <div class="col-lg-6 skill-left" id="skills-language">
                    @foreach($skillsLanguage as $skillLanguage)
                        <div class="skill-single">
                            <p>{{ $skillLanguage->name }} {{ $skillLanguage->percentage }}%</p>
                            <div class="skill-bar skill-bar-progress">
                                <div class="skill-bar-inner"
                                     style="position: absolute; width: {{ $skillLanguage->percentage }}%; height: 100%; background-color: rgb(51, 122, 183)">
                                    <div class="skill-bar-label"
                                         style="padding-left: 5px; line-height: 30px; color: rgb(255, 255, 255);"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-lg-6 skill-left" id="skills-techno">
                    @foreach($skillsTechno as $skillTechno)
                        <div class="skill-single">
                            <p>{{ $skillTechno->name }} {{ $skillTechno->percentage }}%</p>
                            <div class="skill-bar skill-bar-progress">
                                <div class="skill-bar-inner"
                                     style="position: absolute; width: {{ $skillTechno->percentage }}%; height: 100%; background-color: rgb(51, 122, 183)">
                                    <div class="skill-bar-label"
                                         style="padding-left: 5px; line-height: 30px; color: rgb(255, 255, 255);"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
