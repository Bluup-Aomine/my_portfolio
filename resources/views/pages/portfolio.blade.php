@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">Portfolio</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('portfolio') }}">Portfolio</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    @include('component.filtersPortfolio')
@endsection

@section('extra-js')
    <script src="{{ asset('js/front/Project/FilterProjects.js') }}" type="module" defer></script>
    <script src="{{ asset('js/front/Project/LinkTabsProjects.js') }}" type="module" defer></script>
@endsection
