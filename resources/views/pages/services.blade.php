@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">Services</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('services') }}">Services</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="services services-me">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content col-lg-7">
                    <div class="title text-center">
                        <h1 class="mb-10">My Offered Services</h1>
                        <p>
                            At about this time of year, some months after New Year’s resolutions have been made and kept, or
                            made and neglected.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-pie-chart"></span>
                        <a href="#">
                            <h4>Web Design</h4>
                        </a>
                        <p>
                            “It is not because things are difficult that we do not dare; it is because we do not dare that
                            they are difficult.”
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-laptop-phone"></span>
                        <a href="#">
                            <h4>Web Development</h4>
                        </a>
                        <p>
                            If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="services-single">
                        <span class="lnr lnr-tablet"></span>
                        <a href="#">
                            <h4>App Development</h4>
                        </a>
                        <p>
                            Do you sometimes have the feeling that you’re running into the same obstacles over and over again? Many of my conflicts.                    </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
