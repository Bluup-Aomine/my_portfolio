<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/mail.scss') }}">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div id="mail">
        <div class="email-head">
            <div class="email-head-subject">
                <div class="email-title d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <a href="#" class="active">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                 viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                 stroke-linecap="round" stroke-linejoin="round"
                                 class="feather feather-star text-primary-muted">
                                <polygon
                                    points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                            </svg>
                        </span>
                        </a>
                        <span style="margin-top: -5px;">Monsieur '{{ $name }}', vous contacte pour vos services</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="email-body">
            <p style="white-space: pre">{{ $content }}</p>
        </div>
    </div>
</div>
</body>
</html>
