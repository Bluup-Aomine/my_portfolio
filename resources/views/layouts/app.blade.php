<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">

    <!-- LINKS CSS -->
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/app.scss') }}">


    <!-- SCRIPTS JS -->
    <script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js" lang="js" defer></script>
    <script src="{{ asset('js/components/fontAwesome.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" type="module" lang="js" defer></script>
    @yield('extra-js')

    <title>Document</title>
</head>
<body style="display: block">
<button type="button" id="mobile-nav">
    <i class="lnr lnr-menu"></i>
</button>
<header id="header">
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo-header">
                <a href="#">
                    <img src="{{ asset('img/logo.png') }}" alt="image logo header">
                </a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li>
                        <a href="{{ route('index') }}" class="menu-active">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('about') }}">About</a>
                    </li>
                    <li>
                        <a href="{{ route('services') }}">Services</a>
                    </li>
                    <li>
                        <a href="{{ route('portfolio') }}">Portfolio</a>
                    </li>
                    <li>
                        <a href="{{ route('contact') }}">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
    @yield('content')

    @include('component.nav-mobile')
    @include('layouts.footer')
</body>
</html>
