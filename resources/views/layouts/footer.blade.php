<footer class="portfolio-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-5">
                <div class="portfolio-footer-single-widget">
                    <h4>About Me</h4>
                    <p>
                        We have tested a number of registry fix and clean utilities and present our top 3 list on our site for your convenience.
                    </p>
                    <p>
                        Copyright © 2020
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-2 social-widget">
                <div class="portfolio-footer-single-widget">
                    <h4>Follow Me</h4>
                    <p>Let us be social</p>
                    <div class="portfolio-footer-social d-flex align-items-center">
                        <a href="#">
                            <i class="fas fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fas fa-facebook"></i>
                        </a>
                        <a href="#">
                            <i class="fas fa-github"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
