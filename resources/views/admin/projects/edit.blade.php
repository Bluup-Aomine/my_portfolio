@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Edit project</h4>
        </div>
    </div>
    <div id="form-create-project">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form project edit</h4>
                <p class="card-description">
                    Here, you cab edit your project !
                </p>
                @include('admin.projects.form')
            </div>
        </div>
    </div>
@endsection
