<div id="projects-table" class="stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
                <h6 class="card-title mb-0">Projects</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-hover mb-0">
                    <thead>
                    <tr>
                        <th class="pt-0">#</th>
                        <th class="pt-0">Project Name</th>
                        <th class="pt-0">Project Slug</th>
                        <th class="pt-0">Link Web</th>
                        <th class="pt-0">Content</th>
                        <th class="pt-0">Start Date</th>
                        <th class="pt-0">End Date</th>
                        <th class="pt-0">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{ $project->id  }}</td>
                                <td>{{ $project->title }}</td>
                                <td>{{ $project->slug }}</td>
                                <td>{{ $project->link_web }}</td>
                                <td>{{ $project->getContent() }}</td>
                                <td>{{ $project->start_project }}</td>
                                <td>{{ $project->end_project }}</td>
                                <td>
                                    <a href="{{ route('projects.edit', [$project->id]) }}" type="button" class="btn btn-outline-success">Edit</a>
                                    <button type="button" class="btn btn-outline-danger" id="delete-btn">Delete</button>
                                    <form action="{{ route('projects.destroy', [$project]) }}" method="POST" id="delete-form">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
