<form action="{{ $project !== null ? route('projects.update', [$project]) : route('projects.store') }}" method="POST"
      id="form-project" enctype="multipart/form-data">
    @if ($project)
        @method('PUT')
    @endif
    @csrf
    <fieldset>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="title">Title</label>
                <input id="title" type="text" name="title" class="form-control"
                       value="{{$project !== null ? $project->title : null }}" placeholder="Your project title...">
            </div>
            <div class="col-md-6">
                <label for="category">Cateory</label>
                <select name="category_id" id="category" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="start_project">Start Date Project</label>
                <input type="date" name="start_project" id="start_project" class="form-control"
                       value="{{ $project !== null ? $project->start_project : null }}">
            </div>
            <div class="col-md-6">
                <label for="end_project">End Date Project</label>
                <input type="date" name="end_project" id="end_project" class="form-control"
                       value="{{ $project !== null ? $project->end_project : null }}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="back_end">Technologie Back End</label>
                <select name="back_end[]" id="back_end" class="form-control" multiple>
                    @foreach($skillsBack as $skillBack)
                        <option value="{{ $skillBack->name }}">{{ $skillBack->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <label for="front_end">Technologie Front End</label>
                <select name="front_end[]" id="front_end" class="form-control" multiple>
                    @foreach($skillsFront as $skillFront)
                        <option value="{{ $skillFront->name }}">{{ $skillFront->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="link_web">Link to Web</label>
            <input type="text" name="link_web" id="link_web" class="form-control" placeholder="Link to web...">
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" id="content" class="form-control" cols="30"
                      placeholder="Your content..."
                      rows="10">{{ $project !== null ? $project->content : null }}</textarea>
        </div>
        <div class="form-group">
            <label for="conclusion">Conclusion</label>
            <textarea name="conclusion" id="conclusion" class="form-control" cols="30"
                      placeholder="Your content..."
                      rows="10">{{ $project !== null ? $project->conclusion : null }}</textarea>
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" name="image" id="image" class="file-upload-default">
            <div class="input-group col-xs-12">
                <input type="text" class="form-control file-upload-info" placeholder="Upload Image" disabled>
                <span class="input-group-append">
                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                </span>
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
    </fieldset>
</form>

@section('extra-js')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="{{ asset('js/admin/lib/FileUpload.js') }}" type="module" defer></script>
@endsection
