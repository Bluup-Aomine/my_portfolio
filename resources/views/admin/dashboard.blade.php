@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Welcome to Dashboard</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-6 grid-margin stretch-card" id="projects-stats">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h6 class="card-title mb-0">Projects</h6>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-xl-5">
                                    <h3 class="mt-2">{{ $countProjects }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-xl-12">
                                    <div class="mt-md-3" id="chart-projects"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 grid-margin stretch-card" id="skills-stats">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-baseline">
                                <h6 class="card-title mb-0">Skills</h6>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-xl-5">
                                    <h3 class="mt-2">{{ $countSkills }}</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12 col-xl-12">
                                    <div class="mt-md-3" id="chart-skills"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
       <div class="col-md-6">
           @include('admin.projects.projects-table')
       </div>
        <div class="col-md-6">
            @include('admin.skills.skills-table')
        </div>
    </div>
@endsection

@section('extra-js')
<script src="https://cdn.jsdelivr.net/npm/apexcharts" defer></script>
@endsection
