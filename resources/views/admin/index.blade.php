@extends('layouts.app')

@section('content')
    <section class="about-banner">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">Admin</h1>
                    <p class="text-white link-nav">
                        <a href="{{ route('index') }}">Home</a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{ route('admin') }}">Admin</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    @guest()
        <section class="admin-login">
            <div class="container">
                <form action="{{ route('admin.login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control {{ $errors->first('email') ? 'error-field' : '' }}" placeholder="Votre email..." required>
                        <div class="{{ $errors->first('email') ? 'validate' : null }} invalid-feedback">
                            {{ $errors->first('email') ?? null }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" name="password" id="password" class="form-control {{ $errors->first('password') ? 'error-field' : '' }}" placeholder="Votre mot de passe..." required>
                        <div class="{{ $errors->first('password') ? 'validate' : null }} invalid-feedback">
                            {{ $errors->first('password') ?? null }}
                        </div>
                    </div>
                    <button type="submit" class="generic-btn success circle">Envoyer</button>
                </form>
            </div>
        </section>
    @endguest
@endsection
