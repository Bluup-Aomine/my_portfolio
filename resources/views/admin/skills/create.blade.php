@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Create skill</h4>
        </div>
    </div>
    <div id="form-skill-create" class="grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form skill create</h4>
                <p class="card-description">
                    Here, you can create your skill !
                </p>
                @include('admin.skills.form')
            </div>
        </div>
    </div>
@endsection
