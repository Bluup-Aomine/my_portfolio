<div id="skills-table" class="stretch-card pt-3">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
                <h6 class="card-title mb-0">Skills</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-hover mb-0">
                    <thead>
                    <tr>
                        <th class="pt-0">#</th>
                        <th class="pt-0">Skill Name</th>
                        <th class="pt-0">Link</th>
                        <th class="pt-0">Percentage</th>
                        <th class="pt-0">Type</th>
                        <th class="pt-0">Back or Front ?</th>
                        <th class="pt-0">Date</th>
                        <th class="pt-0">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($skills as $skill)
                            <tr>
                                <td>{{ $skill->id }}</td>
                                <td>{{ $skill->name }}</td>
                                <td>{{ $skill->link }}</td>
                                <td>{{ $skill->percentage }}%</td>
                                <td>{{ $skill->type }}</td>
                                <td>{{ $skill->back_or_front }}</td>
                                <td>{{ $skill->created_at }}</td>
                                <td>
                                    <a href="{{ route('skills.edit', [$skill]) }}" type="button" class="btn btn-outline-success">Edit</a>
                                    <button type="button" class="btn btn-outline-danger" id="delete-btn">Delete</button>
                                    <form action="{{ route('skills.destroy', [$skill]) }}" method="POST" id="delete-form">
                                        @method('DELETE')
                                        @csrf
                                    </form>
                                </td>
                            </tr
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
