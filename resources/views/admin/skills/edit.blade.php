@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Edit skill "{{ $skill->name }}"</h4>
        </div>
    </div>
    <div id="form-skill-edit" class="grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form skill edit</h4>
                <p class="card-description">
                    Here, you are edit your skill "{{ $skill->name }}" !
                </p>
                @include('admin.skills.form')
            </div>
        </div>
    </div>
@endsection
