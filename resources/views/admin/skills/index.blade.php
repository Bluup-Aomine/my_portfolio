@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Skills</h4>
        </div>
    </div>
    @include('admin.skills.skills-table')
@endsection
