<form action="{{ $skill !== null ? route('skills.update', [$skill]) : route('skills.store') }}" method="POST" id="form-skill">
    @if ($skill)
        @method('PUT')
    @endif
    @csrf
    <fieldset>
        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" type="text" name="name" class="form-control" value="{{$skill !== null ? $skill->name : null }}" placeholder="Your skill name...">
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <input id="link" type="text" name="link" class="form-control" value="{{$skill !== null ? $skill->link : null }}" placeholder="Your link...">
        </div>
        <div class="form-group">
            <label for="percentage">Percentage</label>
            <input id="percentage" type="number" name="percentage" class="form-control" value="{{ $skill !== null ? $skill->percentage : 50 }}">
        </div>
        <div class="form-group">
            <label for="type">Type</label>
            <select name="type" id="type" class="form-control">
                <option value="language" {{ $skill !== null && $skill->type === 'language' ? 'selected' : null }}>Language</option>
                <option value="techno" {{ $skill !== null && $skill->type === 'techno' ? 'selected' : null }}>Techno</option>
            </select>
        </div>
        <div class="form-group">
            <label for="back_or_front">Back Or Front ?</label>
            <select name="back_or_front" id="back_or_front" class="form-control">
                <option value="back_end" {{ $skill !== null && $skill->back_or_front === 'back_end' ? 'selected' : null }}>Back End</option>
                <option value="front_end" {{ $skill !== null && $skill->back_or_front === 'front_end' ? 'selected' : null }}>Front End</option>
            </select>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
    </fieldset>
</form>
