@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Edit Settings</h4>
        </div>
    </div>
    @include('component.message')
    <div id="settings-edit">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form edit settings</h4>
                <p class="card-description">
                    Here, you can edit your settings !
                </p>
                <form action="{{ route('settings.edit') }}" method="POST" id="form-edit-settings">
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="email">Your Email</label>
                            <input id="email" type="email" name="email" class="form-control"
                                   value="{{ $user->email }}" placeholder="Your email...">
                        </div>
                        <div class="form-group">
                            <label for="username">Your Username</label>
                            <input id="username" type="text" name="username" class="form-control"
                                   value="{{ $user->name }}" placeholder="Your username...">
                        </div>
                        <div class="form-group">
                            <label for="password">Your Password</label>
                            <input type="password" id="password" name="password" class="form-control"
                                   value="{{ $user->getPassword() }}" placeholder="Your password...">
                        </div>
                        <div class="form-group">
                            <label for="new_password">New Password ?</label>
                            <div class="input-group col-xs-12">
                                <input id="new_password" type="password" name="new_password" class="form-control"
                                       value="" placeholder="Your new password...">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" id="password-text-btn">Show</button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="new_password_confirmation">New Password Confirmation ?</label>
                            <div class="input-group col-xs-12">
                                <input id="new_password_confirmation" type="password" name="new_password_confirmation"
                                       class="form-control"
                                       value="" placeholder="Your new password confirmation...">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" id="password-text-btn">Show</button>
                                </span>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
