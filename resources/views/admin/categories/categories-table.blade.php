<div id="categories-table" class="stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline mb-2">
                <h6 class="card-title mb-0">Categories</h6>
            </div>
            <div class="table-responsive">
                <table class="table table-hover mb-0">
                    <thead>
                    <tr>
                        <th class="pt-0">#</th>
                        <th class="pt-0">Category Name</th>
                        <th class="pt-0">Category Slug</th>
                        <th class="pt-0">Category Content</th>
                        <th class="pt-0">Date</th>
                        <th class="pt-0">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id  }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->slug }}</td>
                            <td>{{ $category->content }}</td>
                            <td>{{ $category->created_at }}</td>
                            <td>
                                <a href="{{ route('categories.edit', [$category->id]) }}" type="button" class="btn btn-outline-success">Edit</a>
                                <button type="button" class="btn btn-outline-danger" id="delete-btn">Delete</button>
                                <form action="{{ route('categories.destroy', [$category]) }}" method="POST" id="delete-form">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
