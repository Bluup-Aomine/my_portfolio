@extends('admin.layouts.app')

@section('content')
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">Create Category</h4>
        </div>
    </div>
    <div id="form-create-category">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form category create</h4>
                <p class="card-description">
                    Here, you cab create your category for the projects !
                </p>
                @include('admin.categories.form')
            </div>
        </div>
    </div>
@endsection
