<form action="{{ $category !== null ? route('categories.update', [$category]) : route('categories.store') }}"
      method="POST"
      id="form-project">
    @if ($category)
        @method('PUT')
    @endif
    @csrf
    <fieldset>
        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" type="text" name="name" class="form-control"
                   value="{{$category !== null ? $category->name : null }}" placeholder="Your category name...">
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" id="content" class="form-control" cols="30"
                      rows="10"
                      placeholder="Your category content...">{{ $category !== null ? $category->content : null }}</textarea>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
    </fieldset>
</form>
