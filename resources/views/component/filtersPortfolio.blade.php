<section class="portfolio">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Our Latest Featured Projects</h1>
                    <p>
                        Who are in extremely love with eco friendly system.
                    </p>
                </div>
            </div>
        </div>
        <div class="filters">
            <ul>
                <li class="active filter-projects" data-filter="*" id="filter-projects-all">
                    All
                </li>
                @foreach($categories as $category)
                    <li data-filter="{{ $category->slug }}" id="filter-projects-category" class="filter-projects">
                        {{ $category->name }}
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="filters-content">
            <div class="row grid" id="projects-dev">
                @foreach($projects as $project)
                    <div class="portfolio-single col-sm-4">
                        <div class="relative">
                            <div class="thumb">
                                <div class="overlay overlay-bg"></div>
                                <img
                                    src="{{ $project->image ? $image->thumbImage(asset('image/projects/' . $project->image)) : asset('img/p1.jpg') }}"
                                    alt="project image" class="img-fluid">
                            </div>
                            <a href="{{ route('projects.show', [$project]) }}">
                                <div class="middle">
                                    <div class="text align-self-center d-flex">
                                        <img src="{{ asset('img/preview.png') }}" alt="icon preview hover">
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="p-inner">
                            <h4>{{ $project->title }}</h4>
                            <div class="category">{{ $project->category->name }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
