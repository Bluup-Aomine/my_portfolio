<nav id="mobile-nav-element">
    <ul style="touch-action: pan-y">
        <li>
            <a href="#" class="menu-active">Home</a>
        </li>
        <li>
            <a href="#">About</a>
        </li>
        <li>
            <a href="#">Services</a>
        </li>
        <li>
            <a href="#">Portfolio</a>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
    </ul>
</nav>
<div id="mobile-body-overlay"></div>
