<?php

/** @var Factory $factory */

use App\Category;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    $title = $faker->title;
    return [
        'name' => $faker->title,
        'slug' => Str::slug($title),
        'content' =>  $faker->text
    ];
});
