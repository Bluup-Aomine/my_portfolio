<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Skill;
use Faker\Generator as Faker;

$factory->define(Skill::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'percentage' => $faker->numberBetween(0, 100),
        'type' => 'language'
    ];
});
