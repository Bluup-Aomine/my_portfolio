/**
 * @property {HTMLLinkElement} logout
 * @property {HTMLFormElement} formLogout
 */
export class Logout
{
    constructor() {
        this.logout = document.querySelector('#logout')
        this.formLogout = document.querySelector('#form-logout')
    }

    /**
     * @param {Event} e
     */
    submitForm(e)
    {
        e.preventDefault()
        this.formLogout.submit()
    }

    init()
    {
        this.logout.addEventListener('click', this.submitForm.bind(this))
    }
}
const logout = new Logout()
logout.init()
