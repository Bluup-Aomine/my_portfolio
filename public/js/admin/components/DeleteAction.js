/**
 * @property {HTMLButtonElement} btnsDelete
 */
export class DeleteAction
{
    constructor() {
        this.btnsDelete = document.querySelectorAll('#delete-btn')
    }

    /**
     * @param {Event} e
     */
    actionDelete(e)
    {
        e.preventDefault()

        const parent = e.target.parentElement
        const form = parent.querySelector('#delete-form')
        form.submit()
    }

    init()
    {
        if (this.btnsDelete.length !== 0) {
            this.btnsDelete.forEach((btn) => {
                btn.addEventListener('click', this.actionDelete.bind(this))
            })
        }
    }
}
const deleteAction = new DeleteAction()
deleteAction.init()
