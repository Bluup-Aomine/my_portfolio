export class PasswordText
{
    constructor() {
        this.btnPasswordText = document.querySelectorAll('#password-text-btn')
    }

    /**
     * @param {Event} e
     */
    toggleTypeField(e)
    {
        e.preventDefault()

        const target = e.target
        const parent = target.parentElement.parentElement

        const input = parent.querySelector('input')
        if (input.type === 'password') {
            input.type = 'text'
            target.innerText = 'Hide'
        } else {
            input.type = 'password'
            target.innerText = 'Show'
        }
    }

    init()
    {
        this.btnPasswordText.forEach((btn) => {
            btn.addEventListener('click', this.toggleTypeField.bind(this))
        })
    }
}
const passwordText = new PasswordText()
passwordText.init()
