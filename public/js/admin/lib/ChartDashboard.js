import Ajax from './../../lib/Ajax.js'

/**
 * @property {Ajax} request
 * @property {HTMLDivElement} chartProjects
 * @property {HTMLDivElement} chartsSkills
 * @property {array[]|null} dataPercentage
 * @property {array[]|null} dataTechno
 * @property {array[]|null} dataCount
 * @property {array[]|null} dataYears
 */
export class ChartDashboard {
    constructor() {
        // Modules
        this.request = new Ajax()
        // Element DOM
        this.chartProjects = document.querySelector('#chart-projects')
        this.chartsSkills = document.querySelector('#chart-skills')
        // Array Data Skills
        this.dataPercentage = null
        this.dataTechno = null
        // Array Data Projects
        this.dataCount = null;
        this.dataYears = null
    }

    /**
     * @param {HTMLDivElement} element
     * @param {object} options
     * @return {*}
     */
    initCharts(element, options) {
        let chart = new ApexCharts(element, options);
        return chart.render()
    }

    /**
     * @return {*}
     */
    chartProject() {
        const options = {
            series: [{
                name: "Projects",
                data: this.dataCount
            }],
            chart: {
                type: 'area',
                height: 350,
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },

            title: {
                text: 'Fundamental Analysis of Stocks',
                align: 'left'
            },
            subtitle: {
                text: 'Price Movements',
                align: 'left'
            },
            labels: this.dataYears,
            xaxis: {
                type: 'string',
            },
            yaxis: {
                opposite: true
            },
            legend: {
                horizontalAlign: 'left'
            }
        };
        return this.initCharts(this.chartProjects, options)
    }

    /**
     * @return {*}
     */
    chartSkill() {
        const options = {
            series: [{
                name: 'Capacity percentage',
                data: this.dataPercentage
            }],
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        position: 'top', // top, center, bottom
                    },
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function (val) {
                    return val + "%";
                },
                offsetY: -20,
                style: {
                    fontSize: '12px',
                    colors: ["#304758"]
                }
            },

            xaxis: {
                categories: this.dataTechno,
                position: 'top',
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                crosshairs: {
                    fill: {
                        type: 'gradient',
                        gradient: {
                            colorFrom: '#D8E3F0',
                            colorTo: '#BED1E6',
                            stops: [0, 100],
                            opacityFrom: 0.4,
                            opacityTo: 0.5,
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                }
            },
            yaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false,
                },
                labels: {
                    show: false,
                    formatter: function (val) {
                        return val + "%";
                    }
                }

            },
            title: {
                text: 'Percentage by Techno',
                floating: true,
                offsetY: 330,
                align: 'center',
                style: {
                    color: '#444'
                }
            }
        };
        return this.initCharts(this.chartsSkills, options)
    }

    async setChartDataSkill() {
        let request = await this.request.ajaxURL('/api/skills', 'GET')
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                this.dataPercentage = response.skillsPercentage
                this.dataTechno = response.skillsTechno
                // Charts DOM
                this.chartSkill()
            }
        }
    }

    async setChartDataProject() {
        let request = await this.request.ajaxURL('/api/projects', 'GET')
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                this.dataYears = response.projectsYears
                this.dataCount = response.projectsCount
                // Charts DOM
                this.chartProject()
            }
        }
    }

    init() {
        this.setChartDataSkill()
        this.setChartDataProject()
    }
}

const chartDashboard = new ChartDashboard()
chartDashboard.init()
