/**
 * @property {HTMLBodyElement} body
 * @property {HTMLLinkElement} profileAdmin
 * @property {HTMLDivElement} sidebarToggle
 */
export class Sidebar {

    constructor() {
        this.body = document.querySelector('body')
        this.profileAdmin = document.querySelector('#profileDropdown')
        this.elementProfile = document.querySelector('[aria-labelledby=profileDropdown]')
        this.sidebarToggle = document.querySelector('.sidebar-toggle')
    }

    /**
     * @param {Event} e
     * @param {number} width
     */
    resizeSidebar(e, width) {
        if (window.innerWidth <= width) this.body.classList.add('sidebar-min')
        if (window.innerWidth >= width) this.body.classList.remove('sidebar-min')
    }

    /**
     * @param {Event} e
     * @constructor
     */
    ClickDropdownProfile(e) {
        e.preventDefault()
        let classes = this.elementProfile.classList
        if (!classes.contains('show')) {
            classes.add('show')
        } else {
            classes.remove('show')
        }
    }

    clickSidebarToggle(e) {
        e.preventDefault()

        let classes = this.sidebarToggle.classList
        if (classes.contains('not-active')) {
            classes.add('active')
            classes.remove('not-active')
            this.body.classList.add('sidebar-min')
        } else {
            classes.remove('active')
            classes.add('not-active')
            this.body.classList.remove('sidebar-min')
        }
    }

    init() {
        // Resize event on the page
        window.addEventListener('resize', e => this.resizeSidebar(e, 1200))
        // Click event on the link profile admin
        this.profileAdmin.addEventListener('click', this.ClickDropdownProfile.bind(this))
        // Click event on the button sidebar toggle
        this.sidebarToggle.addEventListener('click', this.clickSidebarToggle.bind(this))
    }
}

const sidebar = new Sidebar()
sidebar.init()
