/**
 * @property {HTMLDivElement} sidebarBody
 * @property {HTMLBodyElement} body
 */
export class SidebarOpen
{
    constructor() {
        this.sidebarBody = document.querySelector('.sidebar-body')
        this.body = document.querySelector('body')
    }

    actionOpenSidebar()
    {
        this.body.classList.add('open-sidebar-min')
    }

    actionCloseSidebar()
    {
        this.body.classList.remove('open-sidebar-min')
    }

    init()
    {
       this.sidebarBody.addEventListener('mouseover', this.actionOpenSidebar.bind(this))
       this.sidebarBody.addEventListener('mouseout', this.actionCloseSidebar.bind(this))
    }
}
const sidebarOpen = new SidebarOpen()
sidebarOpen.init()
