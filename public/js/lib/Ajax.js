export default class Ajax {
    /**
     * @param {string} url
     * @param {string} method
     * @param {FormData|null} formData
     * @return {Promise<Response>}
     */
    ajaxURL(url, method = 'POST', formData = null)
    {
        return fetch(url, {
            method: method,
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
        })
    }
}
