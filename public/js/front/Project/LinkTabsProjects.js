/**
 * @property {HTMLLIElement} linksTab
 */
export class LinkTabsProjects
{
    constructor() {
        this.linksTab = document.querySelectorAll('.filter-projects')
    }

    /**
     * @param {Event} e
     * @param {number} keyLink
     */
    active(e, keyLink)
    {
        e.preventDefault()
        this.linksTab.forEach((value, key) => {
            if (key !== keyLink) {
                value.classList.remove('active')
            }
        })
        const element = e.target
        element.classList.add('active')
    }

    init()
    {
        if (this.linksTab.length !== 0) {
            this.linksTab.forEach((linkTab, key) => {
                linkTab.addEventListener('click', e => this.active(e, key))
            })
        }
    }
}
const linkTabs = new LinkTabsProjects()
linkTabs.init()
