import Ajax from "./../../lib/Ajax.js";

/**
 * @property {Ajax} request
 * @property {HTMLDivElement} projectsBlock
 * @property {HTMLLIElement} filterAll
 * @property {HTMLLIElement} filterCategory
 */
export class FilterProjects {
    constructor() {
        // Modules
        this.request = new Ajax()
        // Elements Block
        this.projectsBlock = document.querySelector('#projects-dev')
        // li Filters
        this.filterAll = document.querySelector('#filter-projects-all')
        this.filterCategory = document.querySelectorAll('#filter-projects-category')
    }

    /**
     * @param {Event} e
     */
    async filterProject(e)
    {
        e.preventDefault()

        const dataFilter = e.target.getAttribute('data-filter')
        const uri = '/api/project/filter/' + dataFilter
        return this.responseFilter(uri)
    }

    /**
     * @param {Event} e
     */
    filterAllProject(e)
    {
        e.preventDefault()

        const uri = '/api/project/filterAll'
        return this.responseFilter(uri)
    }

    /**
     * @param {string} uri
     * @returns {Promise<void>}
     */
    async responseFilter(uri) {
        let request = await this.request.ajaxURL(uri, 'GET')
        if (request.status === 200 || request.status === 302) {
            const response = await request.json()
            if (response.success) {
                const data = response.data

                this.projectsBlock.innerHTML = ''
                data.forEach((element) => {
                    this.projectsBlock.innerHTML += element
                })
            }
        }
    }

    init()
    {
        // Filter Component Category
        this.filterCategory.forEach((li) => {
            li.addEventListener('click', this.filterProject.bind(this))
        })
        // Filter Component All Projects
        this.filterAll.addEventListener('click', this.filterAllProject.bind(this))
    }
}

const filterProjects = new FilterProjects()
filterProjects.init()
