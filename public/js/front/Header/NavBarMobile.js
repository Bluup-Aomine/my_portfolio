/**
 * @property {HTMLButtonElement} btnNavMobile
 * @property {HTMLDivElement} btnNavMobile
 * @property {HTMLBodyElement} btnNavMobile
 */
export class NavBarMobile
{
    constructor() {
        this.btnNavMobile = document.querySelector('#mobile-nav')
        this.overlayNav = document.querySelector('#mobile-body-overlay')
        this.body = document.querySelector('body')
    }

    /**
     * @param {Event} e
     */
    activeNav(e)
    {
        e.preventDefault()

        const target = e.target
        const searchIcon = target.querySelector('i')
        const icon = searchIcon !== null ? searchIcon : target
        if (icon.classList.contains('lnr-menu')) {
            icon.classList.add('lnr-cross')
            icon.classList.remove('lnr-menu')

            this.body.id = 'mobile-nav-active'
            this.overlayNav.style.display = 'block'
        } else {
            icon.classList.add('lnr-menu')
            icon.classList.remove('lnr-cross')

            this.body.id = ''
            this.overlayNav.style.display = 'none'
        }
    }

    init()
    {
        this.btnNavMobile.addEventListener('click', this.activeNav.bind(this))
    }
}
const navBar = new NavBarMobile()
navBar.init()
